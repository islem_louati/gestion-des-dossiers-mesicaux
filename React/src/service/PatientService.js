import axios from "axios";
import authHeader from "./Auth_header";


const  PATIENT_API_BASE_URL ="http://localhost:8095/SpringMVC/allfile";
const  PATIENT2_API_BASE_URL ="http://localhost:8095/SpringMVC/allfiles";
const  AddPATIENT_API_BASE_URL ="http://localhost:8095/SpringMVC/patient/addPatient";
const  GetID_PATIENT_API_BASE_URL ="http://localhost:8095/SpringMVC/patient/getPatientById";
const  updatePATIENT_API_BASE_URL="http://localhost:8095/SpringMVC/patient/updatePatient";
const  deletePATIENT_API_BASE_URL="http://localhost:8095/SpringMVC/patient/deletePatientById";
const  AvecDocteur_API_BASE_URL="http://localhost:8095/SpringMVC/patient/PatientAvecDocteur";

const  SansDocteur_API_BASE_URL="http://localhost:8095/SpringMVC/patient/getPatientSansDocteur";
const  PatientUrgent_API_BASE_URL="http://localhost:8095/SpringMVC/patient/getpatientfiltre";

class PatientService{
        getPatients(){
            return axios.get(PATIENT_API_BASE_URL, { headers: authHeader() });
            
        }
        PatientSansDocteur(){
            return axios.get(SansDocteur_API_BASE_URL, { headers: authHeader() });
            
        }
        PatientUrgent(){
            return axios.get(PatientUrgent_API_BASE_URL, { headers: authHeader() });
            
        }
        patientAvecDocteur(){
            return axios.get(AvecDocteur_API_BASE_URL, { headers: authHeader() });
            
        }
        getPatientss(){
            return axios.get(PATIENT2_API_BASE_URL, { headers: authHeader() });
            
        }
        addPatients(patient){
            return axios.post(AddPATIENT_API_BASE_URL , patient,{headers : authHeader});

        }

        getPatientById(idP){
            return axios.get(GetID_PATIENT_API_BASE_URL+'/'+idP,{headers : authHeader});
        }

        updatePatient(patient,idP){
            return axios.put(updatePATIENT_API_BASE_URL+'/'+idP,patient,{headers : authHeader});
        }
        
        deletePatient(idP){
              return axios.delete(deletePATIENT_API_BASE_URL+'/'+idP,{headers : authHeader});
        }

    }
    export default new PatientService()