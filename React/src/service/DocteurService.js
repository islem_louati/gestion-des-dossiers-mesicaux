import axios from "axios";
import authHeader from "./Auth_header";


const  DOCTEUR_API_BASE_URL ="http://localhost:8095/SpringMVC/docteur/getAllDocteurs";
const  AddDOCTEUR_API_BASE_URL ="http://localhost:8095/SpringMVC/docteur/addDocteur";
const  GetID_DOCTEUR_API_BASE_URL ="http://localhost:8095/SpringMVC/docteur//getDocteurById";
const  updateDOCTEUR_API_BASE_URL="http://localhost:8095/SpringMVC/docteur/updateDocteur";
const  deleteDOCTEUR_API_BASE_URL="http://localhost:8095/SpringMVC/docteur/deleteDocteurById";
const  getdocteurnonenable_API_BASE_URL="http://localhost:8095/SpringMVC/api/v1/registration/nonenable";
    class DocteurService{
        getDocteurs(){
            return axios.get(DOCTEUR_API_BASE_URL, { headers: authHeader() });
            
        }
        getDocteurrs(idP){
            return axios.get(DOCTEUR_API_BASE_URL+'/'+idP, { headers: authHeader() });
            
        }
        getDocteurnonenable(){
            return axios.get(getdocteurnonenable_API_BASE_URL, { headers: authHeader() });
            
        }
        addDocteurs(docteur){
            return axios.post(AddDOCTEUR_API_BASE_URL , docteur,{headers : authHeader});

        }

        getDocteurById(idD){
            return axios.get(GetID_DOCTEUR_API_BASE_URL+'/'+idD,{headers : authHeader});
        }

        updateDocteur(docteur,idD){
            return axios.put(updateDOCTEUR_API_BASE_URL+'/'+idD,docteur,{headers : authHeader});
        }
        
        deleteDocteur(idD){
              return axios.delete(deleteDOCTEUR_API_BASE_URL+'/'+idD,{headers : authHeader});
        }

    }
    export default new DocteurService()