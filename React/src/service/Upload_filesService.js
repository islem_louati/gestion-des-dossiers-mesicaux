import axios from "axios";
import authHeader from "./Auth_header";

const UPLOAD_FILES_URL="http://localhost:8095/SpringMVC/upload";
const Get_FILES_URL="http://localhost:8095/SpringMVC/allfiles";
const  GetID_PATIENT_API_BASE_URL ="http://localhost:8095/SpringMVC/patient/getPatientById";

class Upload_filesService {
    upload(file,idP,onUploadProgress){
     let formData = new FormData ();
     formData.append("file",file);
     console.log(idP)
     return axios.post(UPLOAD_FILES_URL+'/'+idP, formData,
        {
            headers:{
                "Content-Type":"multipart/form-data",
            },
            onUploadProgress,
        });
      
    }
    getPatientById(idP){
      return axios.get(GetID_PATIENT_API_BASE_URL+'/1',{headers : authHeader});
  }
    getFiles() {
      return axios.get(Get_FILES_URL,{headers : authHeader});
    }
  }
  export default new Upload_filesService();