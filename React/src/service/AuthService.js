import axios from "axios";

const API_URL = "http://localhost:8095/SpringMVC/api/v1/registration";

const API_URlL = "http://localhost:8095/SpringMVC/api/v1/registration/registerdocteur";

class AuthService {
  login(username, password) {
    return axios
      .post(API_URL + "/"+"signinn", {
        username,
        password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(username, email, password,appUserRole) {
    return axios.post(API_URL , {
      username,
      email,
      password,
      appUserRole
    });
  }
  registerForDocteur(username, email, password,appUserRole,id) {
    return axios.post(API_URlL+'/' +id , {
      username,
      email,
      password,
      appUserRole
    });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }
}

export default new AuthService();