import React, { Component } from 'react';
import{BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import logo from './logo.svg';
import './App.css';

import EventBus from "./common/EventBus";

import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import AddPatientComponent from './components/AddPatientComponent';
import UpdatePatientComponent from './components/UpdatePatientComponent';
import ViewPatientComponent from './components/ViewPatientComponent';
import UploadFilesComponent from './components/UploadFilesComponent';
import Profile from './components/Profile';
import Login from './components/LoginComponent';
import Register from './components/Registre';
import AuthService from './service/AuthService';
import Forgot from './components/ForgetPass';
import Home from './components/home';
import Layout from './components/Layout';
import ListPatienttComponent from './components/ListePatient_2';
import ListdocteurComponent from './components/ListDocteur';
import Forgetpassword from './components/forgetpaasword';
import updatepassword from './components/updatePassword';
import ListdocteurrComponent from './components/docteur';
import ListfileComponent from './components/ListFiles';
import frontoffice from './components/frontoffice';
import AddDocteurComponent from './components/addDocteur';
import UpdateDocteurComponent from './components/UpdateDocteur';
import docteurnonenableComponent from './components/docteurnonenable';
import RegisterForDocteur from './components/registerforDocteur';
import ListPatientAvecDocteurComponent from './components/PatientAvecDocteur';
import ListPatientSansDocteurComponent from './components/PatientSansDocteur';
import CompleterProfilDocteurComponent from './components/CompleterProfileDocteur';
import profilePatient from './components/ProfilePatient';
import ListdocteurrfrontComponent from './components/docteurfront';
import { LineChart } from './components/line-chart';
import ListPatientUrgentComponent from './components/PatientUrgent';

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showModeratorBoard: false,
      showAdminBoard: false,
      showUserBoard: false,
      currentUser: undefined,
      history:"",
    };
  }
  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showModeratorBoard: user.appUserRole==="ADMIN",
        showAdminBoard:  user.appUserRole==="ADMIN",
        showUserBoard:  user.appUserRole==="ADMIN",
      });
    }
    
    EventBus.on("logout", () => {
      this.logOut();
    });
  }

  componentWillUnmount() {
    EventBus.remove("logout");
  }

  logOut() {
    AuthService.logout();
    this.setState({
      showModeratorBoard: false,
      showAdminBoard: false,
      showUserBoard:false,
      currentUser: undefined,
    });
  }

  render() {
    const { currentUser, showUserBoard,showModeratorBoard, showAdminBoard } = this.state;


  return (
   
    <div>
     



       <Router>
           
                < div >
                  <Switch>
                    <Route path='/' exact component ={Layout}></Route>
              
                    <Route path='/addPatient' component={AddPatientComponent}></Route>
                    <Route path='/UpdatePatient/:id' component={UpdatePatientComponent}></Route>
                    <Route path='/ViewPatient/:id' component={ViewPatientComponent}></Route>
                    <Route path='/upload/:id' component={UploadFilesComponent}></Route>

                    <Route path='/profile' component={Profile}></Route>
                    <Route path='/login' component={Login}></Route>
                    <Route path='/forget' component={Forgot}></Route>
                    <Route path='/registre' component={Register}></Route>
                    <Route path='/home' component={Layout}></Route>

                    <Route path='/p' component={ListPatienttComponent}></Route>

                    <Route path='/forgetpassword' component={Forgetpassword}></Route>

                    <Route path='/docteuur' component={ListdocteurrComponent}></Route>
                    <Route path='/files' component={ListfileComponent}></Route>

                    <Route path='/updatepassword/:id' component={updatepassword}></Route>

                    <Route path='/docteur/:id' component={ListdocteurComponent}></Route>
                    <Route path='/front' component={frontoffice}></Route>
                    <Route path='/addDocteur' component={AddDocteurComponent}></Route>
                    <Route path='/updateDocteur/:id' component={UpdateDocteurComponent}></Route>
                    <Route path='/docteurnonenable' component={docteurnonenableComponent}></Route>
                    <Route path='/registreForDocteur/:id' component={RegisterForDocteur}></Route>
                    <Route path='/patientavecdocteur' component={ListPatientAvecDocteurComponent}></Route>

                    <Route path='/patientsansdocteur' component={ListPatientSansDocteurComponent}></Route>
                    <Route path='/Completerprofiledocteur/:id' component={CompleterProfilDocteurComponent}></Route>
                    <Route path='/profilePatient' component={profilePatient}></Route>

                    <Route path='/docteurfront' component={ListdocteurrfrontComponent}></Route>
                    <Route path='/LineChart' component={LineChart}></Route>

                    <Route path='/patientUrgent' component={ListPatientUrgentComponent}></Route>

                    

                    

                  </Switch>
                   
                    

                  
                 
      
                </div>
           
            </Router>
            
  

            </div>     

    

    
    
  );
}
}
export default App;

