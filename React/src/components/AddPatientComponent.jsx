import axios from 'axios';
import React, { Component } from 'react';
import authHeader from '../service/Auth_header';
import PatientService from '../service/PatientService';

class AddPatientComponent extends Component {
    constructor(props) {
        super(props)

        this.state={
            nom:'',
            prenom:'',
            date_naissance:'',
            adresse:''


        }
        this.changeNomHandler=this.changeNomHandler.bind(this);
        this.changePrenomHandler=this.changePrenomHandler.bind(this);
        this.savePatient=this.savePatient.bind(this);

    }
    savePatient=(p)=>{
        p.preventDefault();
        let patient ={nom:this.state.nom,prenom:this.state.prenom,date_naissance:this.state.date_naissance,adresse:this.state.adresse}
        console.log('patient=>'+JSON.stringify(patient));
        axios.post( `http://localhost:8095/SpringMVC/patient/addPatient`,patient,{ headers: authHeader() });
        console.log(authHeader)
            this.props.history.push('/p');
        ;
    }
    
    changePrenomHandler=(event)=>{
        this.setState({prenom:event.target.value})
    }

    changeNomHandler=(event)=>{
        this.setState({nom:event.target.value})
    }
    changeEmailHandler=(event)=>{
        this.setState({email:event.target.value})
    }
    changeAdresseHandler=(event)=>{
        this.setState({adresse:event.target.value})
    }
    changeDateDeNaissanceHandler=(event)=>{
        this.setState({date_naissance:event.target.value})
    }
    cancel(){
        this.props.history.push('/patients');
    }



    render() {
        return (
            <div> 
                <div className='container'>
                    <div className='row'>
                        <div className='card col-md-6 offset-md-3 offset-md-3'>
                            <h3 className='text-center'> Ajouter Patient</h3>
                            <div className='card-body'>

                                <form>
                                    <div className='form-group'>
                                        <label>nom:</label>
                                        <input placeholder="Nom" name="nom" className="form-control" 
                                          value={this.state.nom} onChange={this.changeNomHandler}/>


                                    </div>
                                    <div className='form-group'>
                                        <label>prenom:</label>
                                        <input placeholder="Prenom" name="prenom" className="form-control" 
                                          value={this.state.prenom} onChange={this.changePrenomHandler}/>


                                    </div>
                                    <div className='form-group'>
                                        <label>email:</label>
                                        <input placeholder="Prenom" name="prenom" className="form-control" 
                                          value={this.state.email} onChange={this.changeEmailHandler}/>


                                    </div>
                                    <div className='form-group'>
                                        <label>Date de Naissance:</label>
                                        <input placeholder="Date de naissance" name="date_naissance" type="date" className="form-control" 
                                          value={this.state.date_naissance} onChange={this.changeDateDeNaissanceHandler}/>


                                    </div>
                                    <div className='form-group'>
                                        <label>Adresse:</label>
                                        <input placeholder="Adresse" name="adresse" className="form-control" 
                                          value={this.state.adresse} onChange={this.changeAdresseHandler}/>


                                    </div>
                           
                                    <button className="btn btn-success" onClick={this.savePatient}>Enregistrer</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Annuler</button>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
                
            </div>
        );
    }
    }



export default AddPatientComponent;