import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

interface Props {
  categories: any[];
  data1: any[];
  data2: any[];
  data3: any[];
}

export class LineChart extends React.Component<Props> {
  get options() {
    return {
      title: {
        text: 'Line chart',
        style: {
          display: 'none'
        }
      },
      legend: {
        align: 'right',
        verticalAlign: 'top',
        symbolWidth: 5,
        symbolHeight: 8
      },
      xAxis: {
        tickColor: '#FFFFFF',
        categories: this.props.categories
      },
      yAxis: {
        tickColor: '#FFFFFF',
        gridLineColor: '#FFFFFF',
        title: {
          style: {
            display: 'none'
          }
        },
        labels: {
          formatter: function () {
            return `$${this.value}`
          }
        }
      },
      plotOptions: {
        series: {
          marker: {
            symbol: 'circle'
          }
        }
      },
      tooltip: {
        shared: true
      },
      series: [{
        type: 'column',
        name: 'Bar',
        color: '#6a6a6a',
        pointPadding: 0,
        groupPadding: 0,
        data: this.props.data3
      }, {
        type: 'line',
        name: 'Line 1',
        color: '#0071ce',
        data: this.props.data1
      }, {
        type: 'line',
        name: 'Line 2',
        color: '#ff671b',
        data: this.props.data2
      }]
    }
  }
  render() {
    return (
      <HighchartsReact
        highcharts={Highcharts}
        options={this.options}
      />
    )
  }
}