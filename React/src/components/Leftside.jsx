import React, { Component } from 'react'  
import { Link } from 'react-router-dom';  
import eventBus from '../common/EventBus';
import AuthService from '../service/AuthService';
export class Leftside extends Component {  
    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);
    
        this.state = {
          showModeratorBoard: false,
          showAdminBoard: false,
          showUserBoard: false,
          currentUser: undefined,
          history:"",
        };
      }
      componentDidMount() {
        const user = AuthService.getCurrentUser();
    
        if (user) {
          this.setState({
            currentUser: user,
            showModeratorBoard: user.appUserRole==="DOCTEUR",
            showAdminBoard:  user.appUserRole==="ADMIN",
            showUserBoard:  user.appUserRole==="Patient",
          });
        }
        
        eventBus.on("logout", () => {
          this.logOut();
        });
      }
    
      componentWillUnmount() {
        eventBus.remove("logout");
      }
    
      logOut() {
        AuthService.logout();
        this.setState({
          showModeratorBoard: false,
          showAdminBoard: false,
          showUserBoard:false,
          currentUser: undefined,
        });
      }

    render() {
        const { currentUser, showUserBoard,showModeratorBoard, showAdminBoard } = this.state;
    console.log(showAdminBoard)
 
        return (  
            <span>
            {showAdminBoard ?  
            <div>  
                <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">  
                    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">  
                        <div class="sidebar-brand-text mx-3">React Routing </div>  
                    </a>  
  
                    <hr class="sidebar-divider my-0" />  
  
                    <li class="nav-item active">  
                        <a class="nav-link" href="index.html">  
                            <i class="fas fa-fw fa-tachometer-alt"></i>  
                            <span>Dashboard</span></a>  
                    </li>  
                    <hr class="sidebar-divider" />  
                    <div class="sidebar-heading">  
                        Interface  
                    </div>  
                 
                    <li class="nav-item">  
                      <Link class="nav-link" to="/docteuur">  <i class="fas fa-fw fa-table"></i>docteur</Link>  
                  </li> 
                  <li class="nav-item">  
                      <Link class="nav-link" to="/docteurnonenable">  <i class="fas fa-fw fa-table"></i>docteur non enable</Link>  
                  </li>   
                    <li class="nav-item">  
                      <Link class="nav-link" to="/p">  <i class="fas fa-fw fa-table"></i>Patients</Link>  
                  </li> 
                  <li class="nav-item">  
                      <Link class="nav-link" to="/patientUrgent">  <i class="fas fa-fw fa-table"></i>cas urgent</Link>  
                  </li> 
                  <li class="nav-item">  
                      <Link class="nav-link" to="/files">  <i class="fas fa-fw fa-table"></i>files</Link>  
                  </li> 
                 
                    <hr class="sidebar-divider" />  
  
                    <div class="sidebar-heading">  
                        Addons  
</div>  
<li class="nav-item">  
                      <Link class="nav-link" to="/files">  <i class="fas fa-fw fa-table"></i>Chat</Link>  
                  </li>  
                    <li class="nav-item">  
                        <Link class="nav-link" to="/color"> <i class="fas fa-fw fa-chart-area"></i>Stat</Link>  
                        {/* <a class="nav-link" href="charts.html"> 
                            <i class="fas fa-fw fa-chart-area"></i> 
                            <span>Charts</span></a> */}  
                    </li>  
                    
                    <hr class="sidebar-divider d-none d-md-block" />  
                    <div class="text-center d-none d-md-inline">  
                        <button class="rounded-circle border-0" id="sidebarToggle"></button>  
                    </div>  
  
                </ul>  
            </div>
              :showModeratorBoard ? 
         <div>  
              <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">  
                  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">  
                      <div class="sidebar-brand-text mx-3">React Routing </div>  
                  </a>  

                  <hr class="sidebar-divider my-0" />  

                  <li class="nav-item active">  
                      <a class="nav-link" href="index.html">  
                          <i class="fas fa-fw fa-tachometer-alt"></i>  
                          <span>Dashboard</span></a>  
                  </li>  
                  <hr class="sidebar-divider" />  
                  <div class="sidebar-heading">  
                      Interface  
                  </div>  
              
                  <li class="nav-item">  
                      <Link class="nav-link" to="/p">  <i class="fas fa-fw fa-table"></i>Patients</Link>  
                  </li> 
                  <li class="nav-item">  
                      <Link class="nav-link" to="/patientUrgent">  <i class="fas fa-fw fa-table"></i>cas urgent</Link>  
                  </li> 
                  <li class="nav-item">  
                      <Link class="nav-link" to="/files">  <i class="fas fa-fw fa-table"></i>files</Link>  
                  </li> 
                  <li class="nav-item">  
                      <Link class="nav-link" to="/patientUrgent">  <i class="fas fa-fw fa-table"></i>cas urgent</Link>  
                  </li> 
                 
                  <hr class="sidebar-divider" />  

                  <div class="sidebar-heading">  
                      Addons  
</div>  
                  <li class="nav-item">  
                      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">  
                          <i class="fas fa-fw fa-folder"></i>  
                          <span>Chat</span>  
                      </a>  
                     
                  </li>  
                  <li class="nav-item">  
                      <Link class="nav-link" to="/color"> <i class="fas fa-fw fa-chart-area"></i>Colors</Link>  
                      {/* <a class="nav-link" href="charts.html"> 
                          <i class="fas fa-fw fa-chart-area"></i> 
                          <span>Charts</span></a> */}  
                  </li>  
                
                  <hr class="sidebar-divider d-none d-md-block" />  
                  <div class="text-center d-none d-md-inline">  
                      <button class="rounded-circle border-0" id="sidebarToggle"></button>  
                  </div>  

              </ul>  
          </div>
              : 
              <div>
              <a href='/front'/>
              </div>
             }
          </span>
           
        )  
    }  
}  
  
export default Leftside  