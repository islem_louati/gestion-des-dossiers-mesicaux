
import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import authHeader from '../service/Auth_header';
import PatientService from "../service/PatientService";
import { Footer } from './Footer';
import HeaderComponent from './HeaderComponent';
import Header from './home';
import Leftside from './Leftside';

class ListFileComponent extends Component {
     constructor(props) {
         super(props)
          this.state={
              patients:[]
              

          }
          this.addPatient = this.addPatient.bind(this);
          this.editPatient=this.editPatient.bind(this);
          this.deletePatient=this.deletePatient.bind(this);

     
     }

     deletePatient(idP){
 
        axios.get(` http://localhost:8095/SpringMVC/patient/getPatientById/${idP}`,{ headers: authHeader() })
        .then(res=>{
          this.setState({patient:res.data});
          console.log(res.data.idP)
      console.log(res.data.files.id);
        axios.delete(` http://localhost:8095/SpringMVC/deletefile/${idP}/${res.data.files.id} `,
        { headers: authHeader() })
       
        window.alert("confirmer la supression ?")
       
        window.location.reload();
      }) }
     viewPatient(id){
         this.props.history.push(`/ViewPatient/${id}`);
     }
     affectDocteur(idP){
        this.props.history.push(`/docteur/${idP}`);
    }
     


     editPatient(id){

        this.props.history.push(`UpdatePatient/${id}`);
     } 
     affectPatient(id){

        this.props.history.push(`docteur/${id}`);
        console.log(id)
     } 
     affectfile(id){

        this.props.history.push(`upload/${id}`);
        console.log(id)
     } 
     
     

      componentDidMount(){
          PatientService.getPatients().then((res)=>{
              this.setState({patients: res.data});
              console.log(res.data)
          }
          );
      }

       addPatient(){
           this.props.history.push('/addPatient');
       }
       

    render() {
        return (
            <div id="wrapper">  
            <Leftside></Leftside>  
           
            <div  class="d-flex navWidth flex-column " style={{ width:'100%'}}>  
                <div id="content">  
                <HeaderComponent></HeaderComponent>
                <div class="container-fluid">

                  <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">table des patients</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="dataTable_length"><label>Show <select name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="dataTable_filter" class="dataTables_filter">
                <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dataTable"/></label></div></div></div><div class="row"><div class="col-sm-12">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style={{width: '100%'}}>
                <thead>
                    <tr role="row"><th class="sorting sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style={{width:'57px'}}>Prenom du patient</th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style={{width: '61px'}}>Nom du Patient</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style={{width:'50px'}}>fichier</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style={{width: '80px'}}>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr><th rowspan="1" colspan="1">Prenom du patient </th><th rowspan="1" colspan="1"> Nom du Patient</th><th rowspan="1" colspan="1">Dossier medical</th>
<th rowspan="1" colspan="1">Actions</th></tr>
                </tfoot>
                <tbody>
    {
        this.state.patients.map(
            patient=>
            <tr key={patient.idF}>
                

                 <td>
                 {patient.name=== null ? ( 
                    <Link onClick={ ()=> this.affectfile(patient.idP)}>pas de dossier medicale</Link>):
                    ( <td>{patient.name}</td>)
                }
                 </td>
                 <td>
                     <button style={{marginLeft: "10px"}} onClick={ () => this.deletePatient(patient.idP)} className="btn btn-danger">Delete file </button>
                 </td>

            </tr>
        )
    }

    
</tbody>
            </table></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="dataTable_previous"><a href="#" aria-controls="dataTable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="dataTable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="dataTable_next"><a href="#" aria-controls="dataTable" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
        </div>
    </div>
</div>

</div> 
                  
                </div>  
                <Footer />  
            </div> 

                </div>
                   );
                }
            }
            
            export default ListFileComponent;