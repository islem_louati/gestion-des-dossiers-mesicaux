import React, { Component, Suspense } from 'react';  

import {  
    Route, Switch, Redirect  
} from 'react-router-dom';  
import { Footer } from './Footer';
import HeaderComponent from './HeaderComponent';
import Header from './home';
import Leftside from './Leftside';

export class Layout extends Component {  
    loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>  
    render() {  
        return (  
            <div>  
                <div id="wrapper">  
                    <Leftside></Leftside>  
                   
                    <div  class="d-flex navWidth flex-column " style={{ width:'100%'}}>  
                        <div id="content">  
                            <Header />  
                           
                        </div>  
                        <Footer />  
                    </div>  
                </div>  
            </div>  
        )  
    }  
}  
  
export default Layout  