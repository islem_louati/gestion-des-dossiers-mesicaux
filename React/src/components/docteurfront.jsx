import React, { Component } from 'react';
import { Link, useParams } from 'react-router-dom';
import DocteurService from '../service/DocteurService';
import PatientService from "../service/PatientService";
import { Footer } from './Footer';
import HeaderComponent from './HeaderComponent';
import Header from './home';
import Leftside from './Leftside';
import Select from 'react-select'
import axios from 'axios';
import authHeader from '../service/Auth_header';
export default class ListdocteurrfrontComponent extends Component {
     constructor(props) {
         super(props)
          this.state={
            id: this.props.match.params.id,
              docteurs:[],
              patients:[],
              selectOptions :[],
              value :[],
              idD:"",
              nom:''

              

          }
     
     }

 



 


  handleChange(e){
    this.setState({idD:e.value, nom:e.label})
   }

 
 
  
   
 

      componentDidMount(){
          DocteurService.getDocteurs().then((res)=>{
              this.setState({docteurs: res.data});
              console.log(res.data)
         
          }
          );
      }
      deleteDocteur(idD){
        axios.delete(`http://localhost:8095/SpringMVC/patient/deleteDocteurById/${idD}`,{ headers: authHeader() });
        window.alert("confirmer la supression ?")
        this.history.push('/docteuur');



     }

     UpdateDocteur(id){
         this.props.history.push(`/updateDocteur/${id}`);
     }

   
       render() {

        return (
            <div id="wrapper">  
            <Leftside></Leftside>  
           
            <div  class="d-flex navWidth flex-column " style={{ width:'100%'}}>  
                <div id="content">  
                <HeaderComponent></HeaderComponent>
                <div class="container-fluid">

                  <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">table des docteur</h6>
    </div>
    <div >
<table>
<tbody>
    {
        this.state.docteurs.map(
            docteur=>
            
    
            <tr key={docteur.idD}>
                    <div class="owl-item active" style={{width: '245px', marginRight: '20px'}}><div class="item-1 h">
            <div class="item-1-contents">
                <tr>
              <th> { docteur.nom}</th>
              </tr>
            
            </div>
            
          </div>
          </div>
                
            </tr>
            
        )
    }
</tbody>

</table>

                <Footer />  
            </div> 

                </div>
                </div>
                </div></div>
                </div>
        )}
        }