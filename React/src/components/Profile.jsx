import axios from "axios";
import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../service/AuthService";
import { Footer } from "./Footer";
import Header from "./home";
import Leftside from "./Leftside";


export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: null,
      userReady: false,
      currentUser: { username: "" }
    };
  }

  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
console.log(currentUser)

    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })   
    axios.get(`http://localhost:8095/SpringMVC/api/v1/registration/usersbyname/${currentUser.id}`).then((res) =>{
    this.setState(({userReady: res.data}))
    console.log(res.data)
  });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    const { currentUser } = this.state;

    return (
      <div id="wrapper">  
      <Leftside>
        
        </Leftside>  
       <div  class="d-flex navWidth flex-column " style={{ width:'100%'}}>  
          <div id="content">  
              <Header />  
              <div class="container d-flex justify-content-center">
	<div class="card mt-5 px-4 pt-4 pb-2">
		<div class="media p-2">
  			<img src="https://imgur.com/yVjnDV8.png" class="mr-1 align-self-start"/>
  			<div class="media-body">
  				<div class="d-flex flex-row justify-content-between">
  					<h6 class="mt-2 mb-0">{currentUser.username}</h6><i class="fas fa-angle-down mr-3 text-muted"></i>
  				</div>
    		
  			</div>
		</div>
		<ul class="list text-muted mt-3 pl-0">
    {(this.state.userReady) ?
        <div>
      
        <p>
          <strong>Email:</strong>{" "}
          {this.state.userReady.email}
        </p>

        
        <strong>Authorities:</strong>
        <ul>
          
        { currentUser.appUserRole === null && currentUser.appUserRole === 'COMMERCIALE' ? 
<a href ={`addcommerciale/${currentUser.id}`}>completer profile </a> : <td>{currentUser.appUserRole}</td>
}
{ this.state.userReady.docteur === null && currentUser.appUserRole === 'DOCTEUR' ? 
<a href ={`Completerprofiledocteur/${currentUser.id}`}>tu dois completer profile </a> : <p>
          <strong>compte:</strong>{" "}
          tu as un profile
        </p>

}
         
        </ul>
      </div>: null}
  		</ul>
	</div>
</div>



          </div>  
        
      </div> 
    

      </div>
    );
  }
}