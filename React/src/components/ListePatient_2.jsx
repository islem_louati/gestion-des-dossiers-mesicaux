
import axios from 'axios';
import React, { Component, useState } from 'react';
import { Link } from 'react-router-dom';
import authHeader from '../service/Auth_header';
import PatientService from "../service/PatientService";
import { Footer } from './Footer';
import HeaderComponent from './HeaderComponent';
import Header from './home';
import Leftside from './Leftside';
import ReactDOM from 'react-dom';
import ReactPaginate from 'react-paginate';

class ListPatienttComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      patients: [],
      totalPatient: 0,
      currentPage: 0



    }
    this.addPatient = this.addPatient.bind(this);
    this.editPatient = this.editPatient.bind(this);
    this.deletePatient = this.deletePatient.bind(this);


  }
  deletePatient(idP){
    axios.post(`http://localhost:8095/SpringMVC/patient/dessafec/${idP}`,{ headers: authHeader() });
    window.alert("confirmer la supression ?")
    axios.delete(`http://localhost:8095/SpringMVC/patient/deletePatientById/${idP}`,{ headers: authHeader() });


 }

 viewPatient(id){
     this.props.history.push(`/ViewPatient/${id}`);
 }
 affectDocteur(idP){
    this.props.history.push(`/docteur/${idP}`);
}
 


 editPatient(id){

    this.props.history.push(`UpdatePatient/${id}`);
 } 
 affectPatient(id){

    this.props.history.push(`docteur/${id}`);
    console.log(id)
 } 
 affectfile(id){

    this.props.history.push(`upload/${id}`);
    console.log(id)
 } 
 


  componentDidMount() {
    PatientService.getPatients().then((res) => {
      this.setState({
        patients: res.data,
        totalPatient: res.data.length
      });
    }
    );
  }

  addPatient() {
    this.props.history.push('/addPatient');
  }

  
    onChangee = async (e) => {
    
      
      let stringKwd = e.target.value
      console.log(stringKwd)
      await axios
        .get(`http://localhost:8095/SpringMVC/allfilesbysearch/${stringKwd}`,{ headers: authHeader()})
        .then((res) => {
          this.setState({
            patients: res.data,
          })
        })
        .catch((e) => {
          if (axios.isCancel(e) || e) {
            console.log('Data not found.')
          }
        })
     
     
      }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.getVal)
  }
  getVal = (e) => {
    if (this.node.current.contains(e.target)) {
      return
    }
    this.setState({
      userList: [],
    })
  }
  setPage = (page) => {
    this.setState({
      currentPage: page
    })
  }
  onChange = async (e) => {
    if (this.cancelToken) {
      this.cancelToken.cancel()
    }
    this.cancelToken = axios.CancelToken.source()
    await axios
      .get(`http://localhost:8095/SpringMVC/patient/getAllPatients`, {
        headers: authHeader()
      })
      .then((res) => {
        console.log(res.data.length, " length")
        this.setState({
          patients: res.data,
        })
      })
      .catch((e) => {
        if (axios.isCancel(e) || e) {
          console.log('Data not found.')
        }
      })
    let stringKwd = e.target.value.toLowerCase()
    console.log(stringKwd)
    let filterData = this.state.patients.filter((item) => {
      return item.prenom.toLowerCase().indexOf(stringKwd) !== -1
    })
    this.setState({
      patients: filterData,
    })

  }



  render() {
    console.log(this.state.totalPatient)
    return (
      <div id="wrapper">
        <Leftside></Leftside>

        <div class="d-flex navWidth flex-column " style={{ width: '100%' }}>
          <div id="content">
            <HeaderComponent></HeaderComponent>
            <div class="container-fluid">

              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">table des patients</h6>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="dataTable_length">
                      </div></div><div class="col-sm-12 col-md-6"><div id="dataTable_filter" class="dataTables_filter">
                      <label>Search:



                        <input
                          type="search" class="form-control form-control-sm" placeholder="recherche par"
                          aria-controls="dataTable"
                          ref={this.node}
                          onClick={this.getVal}
                          onChange={this.onChange}
                        />
                      </label>
                      &nbsp; &nbsp; &nbsp;
              <input
              type="text"
              class="recherche"
              placeholder= "recherche par mot clé"
              ref={this.node}
              onClick={this.getVal}
              onChange={this.onChangee}
            />


                    </div></div></div><div class="row">
                        <div class="col-sm-12">
                          <button type="button" style={{margin : "1rem 0"}} className="btn btn-primary btn-sm" onClick={this.addPatient} > Ajouter Patient</button> <br></br>
                          <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style={{ width: '100%' }}>
                            <thead>
                              <tr role="row"><th class="sorting sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style={{ width: '57px' }}>Prenom du patient</th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style={{ width: '61px' }}>Nom du Patient</th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style={{ width: '49px' }}>Date de naissance</th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style={{ width: '31px' }}>Adresse</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style={{ width: '50px' }}>docteur</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style={{ width: '50px' }}>fichier</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style={{ width: '80px' }}>Action</th>
                              </tr>
                            </thead>
                            <tfoot>
                              <tr><th rowspan="1" colspan="1">Prenom du patient </th><th rowspan="1" colspan="1"> Nom du Patient</th><th rowspan="1" colspan="1">Date de naissance</th><th rowspan="1" colspan="1">Adresse</th><th rowspan="1" colspan="1">docteur</th><th rowspan="1" colspan="1">Actions</th></tr>
                            </tfoot>
                            <tbody>
                              {
                                this.state.patients.slice(this.state.currentPage * 5, (this.state.currentPage + 1) * 5).map(
                                  patientt =>
                                    <tr key={patientt.id}>
                                      <td> {patientt.patient.prenom} </td>
                                      <td> {patientt.patient.nom}</td>
                                      <td> {patientt.patient.date_naissance}</td>

                                      <td>{patientt.patient.adresse}</td>
                                      <td>
                                        {patientt.patient.docteur === null ? (
                                          <Link onClick={() => this.affectDocteur(patientt.patient.idP)}>pas de docteur</Link>) :
                                          (<p>{patientt.patient.docteurs.map((level2) =>
                                            <tr key={level2.idD}>
                                              <p>-{level2.nom}</p>

                                            </tr>
                                          )
                                          }
                                            <Link onClick={() => this.affectDocteur(patientt.patient.idP)}>affecter docteur</Link>
                                          </p>)
                                        }

                                      </td>
                                      <td>
                                        {patientt.name === null ? (
                                          <Link onClick={() => this.affectfile(patientt.patient.idP)}>pas de dossier medicale</Link>) :
                                          (<td>{patientt.name}
                                          <Link onClick={() => this.affectfile(patientt.patient.idP)}>affecter autre fichier medicale</Link></td>
                                          )
                                        }
                                      </td>
                                      <td>
                                        <i class="material-icons" style={{ fontSze: '48px', color: 'red' }} onClick={() => this.deletePatient(patientt.patient.idP)}>delete_sweep</i> &nbsp; &nbsp; &nbsp;
                                        <i class="material-icons" style={{ fontSze: '48px', color: 'red' }} onClick={() => this.editPatient(patientt.patient.idP)}>create</i>
                                      </td>

                                    </tr>
                                )
                              }


                            </tbody>
                          </table></div></div><div class="row">
                        <div class="col-sm-12 col-md-5">
                          <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                            Showing 1 to 10 of 57 entries
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                          <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                            <ReactPaginate
                              breakLabel="..."
                              nextLabel="next >"
                              onPageChange={(e) => this.setPage(e.selected)}
                              pageRangeDisplayed={5}
                              pageCount={Math.ceil(this.state.totalPatient / 5)}
                              previousLabel="< previous"
                              renderOnZeroPageCount={null}
                            />
                          </div></div></div></div>
                  </div>
                </div>
              </div>

            </div>

          </div>
          <Footer />
        </div>

      </div>
    );
  }
}

export default ListPatienttComponent;