import axios from "axios";
import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../service/AuthService";
import "./css/cssprofile.css";

export default class profilePatient extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: null,
      userReady: false,
      currentUser: { username: "" }
    };
  }

  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
console.log(currentUser)

    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })   
    axios.get(`http://localhost:8095/SpringMVC/api/v1/registration/usersbyname/${currentUser.id}`).then((res) =>{
    this.setState(({userReady: res.data}))
    console.log(res.data)
  });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    const { currentUser } = this.state;

    return (
<div>
    <br/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"/>
<div class="container bootstrap snippets bootdey">
<div class="row">
  <div class="profile-nav col-md-3">
      <div class="panel">
          <div class="user-heading round">
              <a href="#">
                  <img src="./assest/img/patient.jpg" alt=""/>
              </a>
              <h1>Camila Smith</h1>
              <p>deydey@theEmail.com</p>
          </div>

          <ul class="nav nav-pills nav-stacked">
              <li class="active"><a href="#"> <i class="fa fa-user"></i> Profile</a></li>
              <li><a href="#"> <i class="fa fa-calendar"></i> Recent Activity <span class="label label-warning pull-right r-activity">9</span></a></li>
              <li><a href="#"> <i class="fa fa-edit"></i> Edit profile</a></li>
          </ul>
      </div>
  </div>
  <div class="profile-info col-md-9">
      <div class="panel">
          
         
      </div>
      <div class="panel">
          
          <div class="panel-body bio-graph-info">
              <h1>Bio Graph</h1>
              <div class="row">
                  <div class="bio-row">
                      <p><span>First Name </span>: Camila</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Last Name </span>: Smith</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Country </span>: Australia</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Birthday</span>: 13 July 1983</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Occupation </span>: UI Designer</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Email </span>: jsmith@flatlab.com</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Mobile </span>: (12) 03 4567890</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Phone </span>: 88 (02) 123456</p>
                  </div>
              </div>
          </div>
      </div>
      <div>
          <div class="row">
              <div class="col-md-6">
                  <div class="panel">
                      <div class="panel-body">
                          <div class="bio-chart">
                              <div style={{display:'inline',width:'100px',height:'100px'}}><canvas width="100" height="100px"></canvas>
                              <input class="knob" data-width="100" data-height="100" data-displayprevious="true" 
                              data-thickness=".2" value="35" data-fgcolor="#e06b7d" data-bgcolor="#e8e8e8" style={{width: '54px', height: '33px', 
                              position: 'absolute', verticalAlign: 'middle', marginTop: '33px', marginLeft: '-77px', border:'0px', fontWeight: 'bold', 
                              fontStyle: 'normal', fontVariant: 'normal', fontStretch: 'normal', fontSize: '20px', lineHeight: 'normal', 
                              fontFamily: 'Arial',
                               textAlign: 'center', color: 'rgb(224, 107, 125)', padding:'0px', WebkitAppearance: 'none', background: 'none'}}/>
                              </div>
                          </div>
                          <div class="bio-desk">
                              <h4 class="red">Envato Website</h4>
                              <p>Started : 15 July</p>
                              <p>Deadline : 15 August</p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="panel">
                      <div class="panel-body">
                          <div class="bio-chart">
                              <div style={{display:'inline',width:'100px',height:'100px'}}><canvas width="100" height="100px"></canvas>
                              <input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" 
                              value="63" data-fgcolor="#4CC5CD" data-bgcolor="#e8e8e8" style={{width: '54px', height: '33px', 
                              position: 'absolute', verticalAlign: 'middle', marginTop: '33px', marginLeft: '-77px', border: '0px', 
                              fontWeight: 'bold', fontStyle: 'normal', fontVariant: 'normal', fontStretch: 'normal', fontSize: '20px', 
                              lineHeight: 'normal', fontFamily: 'Arial', textAlign: 'center', color: 'rgb(76, 197, 205)', padding: '0px', 
                              WebkitAppearance: 'none', background: 'none'}}/>
                              </div>
                          </div>
                          <div class="bio-desk">
                              <h4 class="terques">ThemeForest CMS </h4>
                              <p>Started : 15 July</p>
                              <p>Deadline : 15 August</p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="panel">
                      <div class="panel-body">
                          <div class="bio-chart">
                              <div style={{display:'inline',width:'100px',height:'100px'}}><canvas width="100" height="100px"></canvas>
                              <input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" value="75" 
                              data-fgcolor="#96be4b" data-bgcolor="#e8e8e8" style={{width: '54px', height: '33px', position: 'absolute',
                               verticalAlign: 'middle', marginTop: '33px', marginLeft: '-77px', border: '0px', fontWeight: 'bold', fontStyle: 'normal', 
                               fontVariant: 'normal', fontStretch: 'normal', fontSize: '20px', lineHeight: 'normal', fontFamily: 'Arial', 
                               textAlign: 'center', color: 'rgb(150, 190, 75)', padding: '0px', WebkitAppearance: 'none', background: 'none'}}/></div>
                          </div>
                          <div class="bio-desk">
                              <h4 class="green">VectorLab Portfolio</h4>
                              <p>Started : 15 July</p>
                              <p>Deadline : 15 August</p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="panel">
                      <div class="panel-body">
                          <div class="bio-chart">
                              <div style={{display:'inline',width:'100px',height:'100px'}}><canvas width="100" height="100px"></canvas>
                              <input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" 
                              value="50" data-fgcolor="#cba4db" data-bgcolor="#e8e8e8" style={{width: '54px', height: '33px', position: 'absolute',
                               verticalAlign: 'middle', marginTop: '33px', marginLeft: '-77px', border: '0px', fontWeight: 'bold', fontStyle: 'normal',
                               fontVariant: 'normal', fontStretch: 'normal', fontSize: '20px', lineHeight: 'normal', fontFamily: 'Arial',
                                textAlign: 'center', color: 'rgb(203, 164, 219)', padding: '0px', WebkitAppearance: 'none', background: 'none'}}/></div>
                          </div>
                          <div class="bio-desk">
                              <h4 class="purple">Adobe Muse Template</h4>
                              <p>Started : 15 July</p>
                              <p>Deadline : 15 August</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
</div>
</div>
    );
}
}