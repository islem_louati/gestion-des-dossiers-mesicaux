import React, { Component } from 'react';
import { Link, useParams } from 'react-router-dom';
import DocteurService from '../service/DocteurService';
import PatientService from "../service/PatientService";
import { Footer } from './Footer';
import HeaderComponent from './HeaderComponent';
import Header from './home';
import Leftside from './Leftside';
import Select from 'react-select'
import axios from 'axios';
import authHeader from '../service/Auth_header';
export default class ListdocteurComponent extends Component {
     constructor(props) {
         super(props)
          this.state={
            id: this.props.match.params.id,
              docteurs:[],
              patients:[],
              selectOptions :[],
              value :[],
              idD:"",
              nom:''

              

          }
     
     }

     deletePatient(idP){
      
      axios.delete(`http://localhost:8095/SpringMVC/docteur/deleteDocteurById/${idP}`,{ headers: authHeader() });
      window.alert("confirmer la supression ?")


   }



    async getOptions(){
      const res = await axios.get(`http://localhost:8095/SpringMVC/docteur/getAllDocteurs/${this.state.id}`,{ headers: authHeader() });
      const data = res.data
  
      const options = data.map(d => ({
        "value" : d.idD,
        "label" : d.nom
  
      }))
  
      this.setState({selectOptions: options})

    }


  handleChange(e){
    this.setState({idD:e.value, nom:e.label})
   }

 
   affectDocteurToPatient(idD){
  
    

    console.log(this.state.id); 
  
   
    axios.post(`http://localhost:8095/SpringMVC/docteur/affectpatientTodoctor/${this.state.id}/${idD}`,{ headers: authHeader() })
    .then((response) => {
      this.setState({ patient: response.data });
      
    })
   window.alert("client affecter")
    
  console.log("aaaaaaa")
  this.props.history.push('/p');

      
 }

      componentDidMount(){
          axios.get(`http://localhost:8095/SpringMVC/docteur/getAllDocteurs/${this.state.id}`,{ headers: authHeader() })
          .then((res)=>{
              this.setState({docteur: res.data});
              this.getOptions()
          }
          );
      }

   
       render() {
        console.log(this.state.selectOptions)

        return (
            <div>
                <div class="row mrgn">
             <div class="col-12">
             <div>
        <Select options={this.state.selectOptions} onChange={this.handleChange.bind(this)} onClick={ () => this.affectComerciale(this.state.idD)}/>
    <p>You have selected <strong>{this.state.nom}</strong> whose id is <strong>{this.state.idD}</strong>
    <Link onClick={ () => this.affectDocteurToPatient(this.state.idD)} >affecter
                                                                 </Link>
    </p>

      </div>
    
             
                </div>
                </div>

            </div>
        )
       }
    }