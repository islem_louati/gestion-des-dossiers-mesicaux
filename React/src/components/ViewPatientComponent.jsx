import React, { Component } from 'react';
import PatientService from '../service/PatientService';

class ViewPatientComponent extends Component {
    constructor(props){
        super(props)

        this.state={
            id:this.props.match.params.id,
            patient:{}
            

        }
    }
    componentDidMount(){
        PatientService.getPatientById(this.state.id).then(res=>{
            this.setState({patient:res.data});
        })

    }

    render() {
        return (
            <div>
            <br></br>
            <div className = "card col-md-6 offset-md-3">
                <h3 className = "text-center">  Détails du Patient</h3>
                <div className = "card-body">
                    <div className = "row">
                        <label> Nom du Patient: </label>
                        <div> { this.state.patient.nom }</div>
                        </div>
                    <div className = "row">
                        <label> Prenom du Patient: </label>
                        <div> { this.state.patient.prenom}</div>
                        </div>
                    <div className = "row">
                        <label> Date de Naissance du Patient: </label>
                        <div> { this.state.patient.date_naissance}</div>
                    </div>
                    <div className = "row">
                        <label> Adresse du Patient: </label>
                        <div> { this.state.patient.adresse}</div>
                    </div>
                </div>

            </div>
        </div>
    )
}
}

export default ViewPatientComponent;