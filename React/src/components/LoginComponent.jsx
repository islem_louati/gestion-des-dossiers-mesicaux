import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import AuthService from "../service/AuthService";



const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
      username: "",
      password: "",
      loading: false,
      message: ""
    };
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.login(this.state.username, this.state.password).then(
        () => {
          this.props.history.push("/profile");
          window.location.reload();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    return (
      <body class="bg-gradient-primary">
    <div class="container">
      <div class="row justify-content-center">
      <div className="col-md-12">
        <div className="card card-container">
    
          <Form onSubmit={this.handleLogin}
            ref={c => { this.form = c; }}  >
            <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block ">
                            <img src="./assest/img/stafff.jpg" alt="Image" class="img-fluid"/>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter username ..."   name="username"
                value={this.state.username}
                onChange={this.onChangeUsername}
                validations={[required]}/>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password"  name="password"
                value={this.state.password}
                onChange={this.onChangePassword}
                validations={[required]}/>
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck"/>
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div>
                                       
                                       
                                    </form>
                                    
                                   
                                        
                                        <a href="index.html" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> Login with Google
                                        </a>
                                        <a href="index.html" class="btn btn-facebook btn-user btn-block">
                                            <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                                        </a>
                                    <div class="text-center">
                                        <a class="small" href="/forgetpassword">Forgot Password?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="/registre">Create an Account!</a>
                                    </div>
                                    <div className="form-group">
              <button
                className="btn btn-primary btn-block mt-5"
                disabled={this.state.loading}
              >
                {this.state.loading && (
                  <span class="btn btn-primary btn-user btn-block"></span>
                )}
                <span>Login</span>
              </button>
            
            </div>

                                </div>
                            </div>
                        </div>
                    </div>

      
            {this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
        </div>
      </div>
      </div>
      </div>
      </body>
    );
  }
}
