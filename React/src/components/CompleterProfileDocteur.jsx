import axios from 'axios';
import React, { Component } from 'react';
import authHeader from '../service/Auth_header';
import PatientService from '../service/PatientService';
import HeaderComponent from './HeaderComponent';
import Leftside from './Leftside';

class CompleterProfilDocteurComponent extends Component {
    constructor(props) {
        super(props)

        this.state={
            nom:'',
            prenom:'',
            email:'',
            specialité:'',
            téléphone:''


        }
        this.changeNomHandler=this.changeNomHandler.bind(this);
        this.changePrenomHandler=this.changePrenomHandler.bind(this);
        this.saveDocteur=this.saveDocteur.bind(this);

    }
    saveDocteur=(p)=>{
        p.preventDefault();
        let docteur ={nom:this.state.nom,prenom:this.state.prenom,email:this.state.email,specialité:this.state.specialité,téléphone:this.state.téléphone}
        console.log('patient=>'+JSON.stringify(docteur));
        axios.post( `http://localhost:8095/SpringMVC/docteur/addDocteur/${this.props.match.params.id}`,docteur,{ headers: authHeader() });
        console.log(authHeader)
            this.props.history.push('/docteuur');
        ;
    }
    
    changePrenomHandler=(event)=>{
        this.setState({prenom:event.target.value})
    }

    changeNomHandler=(event)=>{
        this.setState({nom:event.target.value})
    }
    changeEmailHandler=(event)=>{
        this.setState({email:event.target.value})
    }
    changeSpecialiteHandler=(event)=>{
        this.setState({specialité:event.target.value})
    }
    changeTelephoneHandler=(event)=>{
        this.setState({téléphone:event.target.value})
    }
    cancel(){
        this.props.history.push('/docteuur');
    }



    render() {
        return (
            <div id="wrapper"> 
                 
            <Leftside></Leftside>  
           
            <div  class="d-flex navWidth flex-column " style={{ width:'100%'}}>  
                <div id="content">  
                <HeaderComponent></HeaderComponent>
                <div className='container'>
                    <div className='row'>
                        <div className='card col-md-6 offset-md-3 offset-md-3'>
                            <h3 className='text-center'> Ajouter Patient</h3>
                            <div className='card-body'>

                                <form>
                                    <div className='form-group'>
                                        <label>nom:</label>
                                        <input placeholder="Nom" name="nom" className="form-control" 
                                          value={this.state.nom} onChange={this.changeNomHandler}/>


                                    </div>
                                    <div className='form-group'>
                                        <label>prenom:</label>
                                        <input placeholder="Prenom" name="prenom" className="form-control" 
                                          value={this.state.prenom} onChange={this.changePrenomHandler}/>


                                    </div>
                                    <div className='form-group'>
                                        <label>email:</label>
                                        <input placeholder="email" name="email" className="form-control" 
                                          value={this.state.email} onChange={this.changeEmailHandler}/>


                                    </div>
                                    <div className='form-group'>
                                        <label>Spécialite:</label>
                                        <input placeholder="Spécialite" name="Spécialite" type="text" className="form-control" 
                                          value={this.state.specialité} onChange={this.changeSpecialiteHandler}/>


                                    </div>
                                    <div className='form-group'>
                                        <label>Télephone:</label>
                                        <input placeholder="Telephone" name="Telephone" className="form-control" 
                                          value={this.state.téléphone} onChange={this.changeTelephoneHandler}/>


                                    </div>
                           
                                    <button className="btn btn-success" onClick={this.saveDocteur}>Enregistrer</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Annuler</button>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
                
            </div>
            </div>
            </div>
        );
    }
    }



export default CompleterProfilDocteurComponent;