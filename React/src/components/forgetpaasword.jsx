import React, { Component } from 'react';
import { Link, useParams } from 'react-router-dom';
import DocteurService from '../service/DocteurService';
import PatientService from "../service/PatientService";
import { Footer } from './Footer';
import HeaderComponent from './HeaderComponent';
import Header from './home';
import Leftside from './Leftside';
import Select from 'react-select'
import axios from 'axios';
import authHeader from '../service/Auth_header';
export default class Forgetpassword extends Component {
     constructor(props) {
         super(props)
          this.state={
            id: this.props.match.params.id,
          
              email:''

              

          }
     
     }

 
     changeemailHandler=(event)=>{
        this.setState({email:event.target.value})
    }
     forgetpassword=(p)=>{
        p.preventDefault();
        let patient ={email:this.state.email}
        console.log('patient=>'+JSON.stringify(patient));
        axios.get( `http://localhost:8095/SpringMVC/api/v1/registration/emailforgt/${this.state.email}`,{ headers: authHeader() });
        console.log(authHeader)
        ;
        window.alert("confirmer  ?")
        window.location.reload(false);

    }

   
  
 

      

   
       render() {
        console.log(this.state.selectOptions)

        return (
<body class="bg-gradient-primary">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block ">
                            <img src="./assest/img/stafff.jpg" alt="Image" class="img-fluid"/>

                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                                        <p class="mb-4">We get it, stuff happens. Just enter your email address below
                                            and we'll send you a link to reset your password!</p>
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user" 
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                             placeholder="Enter Email Address..."
                                             value={this.state.email} onChange={this.changeemailHandler}/>
                                        </div>
                                        <button class="btn btn-primary btn-user btn-block"
                                         onClick={this.forgetpassword}> Reset Password</button>

                                      
                                    </form>
                                    
                                    <div class="text-center">
                                        <a class="small" href="/registre">Create an Account!</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="/login">Already have an account? Login!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>


    <script src="js/sb-admin-2.min.js"></script>



</body>
)
}
}