import React, { Component } from 'react';
import { Link, useParams } from 'react-router-dom';
import DocteurService from '../service/DocteurService';
import PatientService from "../service/PatientService";
import { Footer } from './Footer';
import HeaderComponent from './HeaderComponent';
import Header from './home';
import Leftside from './Leftside';
import Select from 'react-select'
import axios from 'axios';
import authHeader from '../service/Auth_header';
export default class updatepassword extends Component {
     constructor(props) {
         super(props)
          this.state={
            id: this.props.match.params.id,
          
              password:''

              

          }
     
     }

 
     changepasswordHandler=(event)=>{
        this.setState({password:event.target.value})
    }
     forgetpassword=(p)=>{
        p.preventDefault();
        let patient ={password:this.state.password}
        console.log('patient=>'+JSON.stringify(patient));
        axios.post( `http://localhost:8095/SpringMVC/api/v1/registration/updatepassword/${this.state.id}`,patient,{ headers: authHeader() });
        console.log(authHeader)
        ;
        window.alert("confirmer  ?")
        this.props.history.push('/login');
    }

   
  
 

      

   
       render() {
        console.log(this.state.selectOptions)

        return (
<body class="bg-gradient-primary">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                                        <p class="mb-4">We get it, stuff happens. Just enter your email address below
                                            and we'll send you a link to reset your password!</p>
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" 
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                             placeholder="Entrer votre mot de passe ..."
                                             value={this.state.password} onChange={this.changepasswordHandler}/>
                                        </div>
                                        <button class="btn btn-primary btn-user btn-block"
                                         onClick={this.forgetpassword}> Reset Password</button>

                                      
                                    </form>
                                    
                                    <div class="text-center">
                                        <a class="small" href="register.html">Create an Account!</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="login.html">Already have an account? Login!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>


    <script src="js/sb-admin-2.min.js"></script>



</body>
)
}
}