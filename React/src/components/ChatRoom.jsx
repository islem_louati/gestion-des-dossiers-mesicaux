import event from 'eventbus/lib/event';
import React from 'react';
import { useState } from 'react';
import { over } from 'stompjs';
import SockJS from 'sockjs-client';

var stompClient=null;
const ChatRoom =()=> {
    const[userData,setUserData]=useState({
        username:"",
        receivername:"",
        connected:false,
        message:"",

    const handleUserName =(event)=>{
        const{value}=event.target;
        setUserData({...userData,"username":value});

    }
    const registerUser=()=>{
        let Sock = new SockJS('http://localhost:8095/SpringMVC/ws')
        stompClient=over(Sock);
        stompClient.connect({},onConnected,onError);

    }
    const onConnected=()=>{
        setUserData({...userData,"username":true});
        stompClient.su

    }



    })
        return (
            <div className="Container">
                {userData.connected?
                <div>
                </div>
                :
                <div className='register'>
                    <input
                    id='user-name'
                    placeholder='Enter the user name'
                    value={userData.username}
                    onChange={handleUserName}
                />
                <button type='button' onClick={registerUser}> connect</button>
                
                
                
            </div>}
            </div>
        )
    }


export default ChatRoom;