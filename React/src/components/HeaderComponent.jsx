import React, { Component } from 'react'
import eventBus from '../common/EventBus';
import AuthService from '../service/AuthService';
import Header from './home';

class HeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);
    
        this.state = {
          showModeratorBoard: false,
          showAdminBoard: false,
          showUserBoard: false,
          currentUser: undefined,
          history:"",
        };
      }
      componentDidMount() {
        const user = AuthService.getCurrentUser();
    
        if (user) {
          this.setState({
            currentUser: user,
            showModeratorBoard: user.appUserRole==="ADMIN",
            showAdminBoard:  user.appUserRole==="ADMIN",
            showUserBoard:  user.appUserRole==="ADMIN",
          });
        }
        
        eventBus.on("logout", () => {
          this.logOut();
        });
      }
    
      componentWillUnmount() {
        eventBus.remove("logout");
      }
    
      logOut() {
        AuthService.logout();
        this.setState({
          showModeratorBoard: false,
          showAdminBoard: false,
          showUserBoard:false,
          currentUser: undefined,
        });
      }

    render() {
        const { currentUser, showUserBoard,showModeratorBoard, showAdminBoard } = this.state;
    console.log(currentUser)
    
      return (
    
     
        <div>
      
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">  
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">  
  <i class="fa fa-bars"></i>  
</button>  
  
  
<form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">  
  <div class="input-group">  
    
  </div>  
</form>  
<ul class="navbar-nav ml-auto">  
  
  
  <li class="nav-item dropdown no-arrow d-sm-none">  
    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  
      <i class="fas fa-search fa-fw"></i>  
    </a>  
   
    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">  
      <form class="form-inline mr-auto w-100 navbar-search">  
        <div class="input-group">  
          <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2"/>  
          <div class="input-group-append">  
            <button class="btn btn-primary" type="button">  
              <i class="fas fa-search fa-sm"></i>  
            </button>  
          </div>  
        </div>  
      </form>  
    </div>  
  </li>  
  
   
  <li class="nav-item dropdown no-arrow mx-1">  
   
  
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">  
      <h6 class="dropdown-header">  
        Alerts Center  
      </h6>  
      <a class="dropdown-item d-flex align-items-center" href="#">  
        <div class="mr-3">  
          <div class="icon-circle bg-primary">  
            <i class="fas fa-file-alt text-white"></i>  
          </div>  
        </div>  
        <div>  
          <div class="small text-gray-500">December 12, 2019</div>  
          <span class="font-weight-bold">A new monthly report is ready to download!</span>  
        </div>  
      </a>  
      <a class="dropdown-item d-flex align-items-center" href="#">  
        <div class="mr-3">  
          <div class="icon-circle bg-success">  
            <i class="fas fa-donate text-white"></i>  
          </div>  
        </div>  
        <div>  
          <div class="small text-gray-500">December 7, 2019</div>  
          $290.29 has been deposited into your account!  
        </div>  
      </a>  
      <a class="dropdown-item d-flex align-items-center" href="#">  
        <div class="mr-3">  
          <div class="icon-circle bg-warning">  
            <i class="fas fa-exclamation-triangle text-white"></i>  
          </div>  
        </div>  
        <div>  
          <div class="small text-gray-500">December 2, 2019</div>  
          Spending Alert: We've noticed unusually high spending for your account.  
        </div>  
      </a>  
      <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>  
    </div>  
  </li>  
  
  
  <li class="nav-item dropdown no-arrow mx-1">  
  
  
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">  
      <h6 class="dropdown-header">  
        Message Center  
      </h6>  
      <a class="dropdown-item d-flex align-items-center" href="#">  
        <div class="dropdown-list-image mr-3">  
          <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt=""/>  
          <div class="status-indicator bg-success"></div>  
        </div>  
        <div class="font-weight-bold">  
          <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>  
          <div class="small text-gray-500">Emily Fowler · 58m</div>  
        </div>  
      </a>  
      <a class="dropdown-item d-flex align-items-center" href="#">  
        <div class="dropdown-list-image mr-3">  
          <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt=""/>  
          <div class="status-indicator"></div>  
        </div>  
        <div>  
          <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>  
          <div class="small text-gray-500">Jae Chun · 1d</div>  
        </div>  
      </a>  
      <a class="dropdown-item d-flex align-items-center" href="#">  
        <div class="dropdown-list-image mr-3">  
          <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt=""/>  
          <div class="status-indicator bg-warning"></div>  
        </div>  
        <div>  
          <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>  
          <div class="small text-gray-500">Morgan Alvarez · 2d</div>  
        </div>  
      </a>  
      <a class="dropdown-item d-flex align-items-center" href="#">  
        <div class="dropdown-list-image mr-3">  
          <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt=""/>  
          <div class="status-indicator bg-success"></div>  
        </div>  
        <div>  
          <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>  
          <div class="small text-gray-500">Chicken the Dog · 2w</div>  
        </div>  
      </a>  
      <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>  
    </div>  
  </li>  
  
  </ul>
              <div >
              {showUserBoard && (
                  <li className="nav-item">
                   
                   <a href="/front" className="nav-link">
                   </a>
                  </li>
                )}
    
    
                {showAdminBoard && (
                  
                  
                    <a href="/candidat" className="nav-link">
                     
                    </a>
                 
                )}
    
                {showModeratorBoard && (
                  <li className="nav-item">
                    <a href="/candidat" className="nav-link">
                     
                     </a>
                  </li>
                )}
              </div>
    
              {currentUser ? (
                <div className="navbar-nav ml-auto">
                  <li className="nav-item">
                  <a href="/profile" className="nav-link">
                     
                     
                      {currentUser.username}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/login" className="nav-link" onClick={this.logOut}>
                      LogOut
                    </a>
                  </li>
                </div>
              ) : (
                <div className="navbar-nav ml-auto">
                  <li className="nav-item">
                  <a href="/login" className="nav-link">
                     
                    
                      Login
                     </a>
                  </li>
    
                  <li className="nav-item">
                    <a href="/login" className="nav-link">
                      Sign Up
                    </a>
                  </li>
                </div>
              )}
            </nav>
        
    </div>
      )
}
}

export default HeaderComponent