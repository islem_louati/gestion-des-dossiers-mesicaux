import React, { Component } from 'react';
import { Link, useParams } from 'react-router-dom';
import DocteurService from '../service/DocteurService';
import PatientService from "../service/PatientService";
import { Footer } from './Footer';
import HeaderComponent from './HeaderComponent';
import Header from './home';
import Leftside from './Leftside';
import Select from 'react-select'
import axios from 'axios';
import authHeader from '../service/Auth_header';
import ReactPaginate from 'react-paginate';
export default class ListdocteurrComponent extends Component {
     constructor(props) {
         super(props)
          this.state={
            id: this.props.match.params.id,
              docteurs:[],
              patients:[],
              selectOptions :[],
              value :[],
              idD:"",
              nom:'',
              totalDocteur: 0,
              currentPage: 0

              

          }
     
     }

 



 


  handleChange(e){
    this.setState({idD:e.value, nom:e.label})
   }

 
 
  
   
 

      componentDidMount(){
          DocteurService.getDocteurs().then((res)=>{
              this.setState({docteurs: res.data,
                totalDocteur: res.data.length
            });
              console.log(res.data);
         
          }
          );
      }
      deleteDocteur(idD){
        axios.delete(`http://localhost:8095/SpringMVC/patient/deleteDocteurById/${idD}`,{ headers: authHeader() });
        window.alert("confirmer la supression ?")
        this.history.push('/docteuur');



     }

     UpdateDocteur(id){
         this.props.history.push(`/updateDocteur/${id}`);
     }

   
       render() {

        return (
            <div id="wrapper">  
            <Leftside></Leftside>  
           
            <div  class="d-flex navWidth flex-column " style={{ width:'100%'}}>  
                <div id="content">  
                <HeaderComponent></HeaderComponent>
                <div class="container-fluid">

                  <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">table des docteur</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="dataTable_length">
                </div></div><div class="col-sm-12 col-md-6">
                    <div id="dataTable_filter" class="dataTables_filter">
                <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dataTable"/></label></div></div></div><div class="row"><div class="col-sm-12">
                <a  type="button" className="btn btn-primary btn-sm"  href='/addDocteur' > Ajouter docteur</a> <br></br>
                <br></br>
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style={{width: '100%'}}>
              
                <tfoot>
                    <tr><th rowspan="1" colspan="1">nom du docteur </th>
                    <th rowspan="1" colspan="1"> Prénom du docteur</th>
                    <th rowspan="1" colspan="1">spécialité</th><th rowspan="1" colspan="1">téléphone</th><th rowspan="1" colspan="1">Actions</th></tr>
                </tfoot>
                
                <thead>
                    <tr role="row">
                    <th class="sorting sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style={{width:'90px'}}>nom du docteur</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style={{width: '91px'}}>prenom du docteur</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style={{width: '99px'}}>specialité</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style={{width: '69px'}}>téléphone</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style={{width: '90px'}}>Action</th>
                    </tr>
                </thead>
                <tbody>
    {
        this.state.docteurs.map(
            docteur=>
            <tr key={docteur.idD}>
                 <td> { docteur.nom} </td>  
                <td> { docteur.prenom} </td>   
                 <td> {docteur.specialité}</td>
                 <td> {docteur.téléphone}</td>
               
            
                 <td>
                     <i class="material-icons" style={{fontSze:'48px',color:'red'}} onClick={ () => this.deleteDocteur(docteur.idD)}>delete_sweep</i> &nbsp; &nbsp; &nbsp;
                     <i class="material-icons" style={{fontSze:'48px',color:'red'}} onClick={()=>this.UpdateDocteur(docteur.idD)}>create</i>

                     <button style={{marginLeft: "10px"}} onClick={ () => this.viewPatient(docteur.id)} className="btn btn-info">détails </button>
                 </td>

            </tr>
        )
    }

    
</tbody>
            </table></div></div>
            <div class="row">
                        <div class="col-sm-12 col-md-5">
                          <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                            Showing 1 to 10 of 57 entries
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                          <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                            <ReactPaginate
                              breakLabel="..."
                              nextLabel="next >"
                              onPageChange={(e) => this.setPage(e.selected)}
                              pageRangeDisplayed={5}
                              pageCount={Math.ceil(this.state.totalDocteur / 5)}
                              previousLabel="< previous"
                              renderOnZeroPageCount={null}
                            />
                          </div></div></div></div>
                  </div>
                </div>
              </div>

            </div>

          </div>
          <Footer />
        </div>

      </div>
    );
  }
}