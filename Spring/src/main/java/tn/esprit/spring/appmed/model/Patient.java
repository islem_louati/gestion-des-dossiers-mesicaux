package tn.esprit.spring.appmed.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Patient implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idP;
	
	private String nom;
	private Boolean Urgent = false;
	private String prenom;
	private String email;
	private String maladie;
	private int Age;
	
	public String getMaladie() {
		return maladie;
	}




	public void setMaladie(String maladie) {
		this.maladie = maladie;
	}




	public int getAge() {
		return Age;
	}




	public void setAge(int age) {
		Age = age;
	}




	public AppUser getAppuser() {
		return appuser;
	}




	public void setAppuser(AppUser appuser) {
		this.appuser = appuser;
	}


	@OneToOne(mappedBy = "patient")
	@JsonIgnore
	private AppUser appuser;

	public Patient(String nom, String prenom, String email, Date date_naissance, String adresse) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.date_naissance = date_naissance;
		this.adresse = adresse;
	}




	public Boolean getUrgent() {
		return Urgent;
	}




	public void setUrgent(Boolean urgent) {
		Urgent = urgent;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}


	@Temporal(TemporalType.DATE)
	private Date date_naissance;
	
	private String adresse;
	
	  @ManyToMany(fetch = FetchType.LAZY,
		      cascade = {
		          CascadeType.PERSIST,
		          CascadeType.MERGE
		      })
		  @JoinTable(name = "patient_docteurs",
		        joinColumns = { @JoinColumn(name = "idP") },
		        inverseJoinColumns = { @JoinColumn(name = "idD") })
		  private Set<Docteur> docteurs = new HashSet<>();
	  
	  @OneToMany(mappedBy = "patient" ,fetch = FetchType.EAGER)
	//  @JsonIgnore
	    private List<FileDB> files; 
	   
	
	public int getIdP() {
		return idP;
	}




	public void setIdP(int idP) {
		this.idP = idP;
	}
















	public Set<Docteur> getDocteurs() {
		return docteurs;
	}




	@Override
	public String toString() {
		return "Patient [idP=" + idP + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", date_naissance="
				+ date_naissance + ", adresse=" + adresse + ", docteurs=" + docteurs + "]";
	}




	public void setDocteurs(Set<Docteur> docteurs) {
		this.docteurs = docteurs;
	}









	public List<FileDB> getFiles() {
		return files;
	}




	public void setFiles(List<FileDB> files) {
		this.files = files;
	}




	public Patient() {
		this.idP = 0;
		this.nom = "";
		this.prenom = "";
		this .adresse = "";
	}

	
	
	
	public Patient(int id, String nom, String prenom, Date date_naissance, String adresse) {
		super();
		this.idP = id;
		this.nom = nom;
		this.prenom = prenom;
		this.date_naissance = date_naissance;
		this.adresse = adresse;
	}






	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public Date getDate_naissance() {
		return date_naissance;
	}


	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	


}
