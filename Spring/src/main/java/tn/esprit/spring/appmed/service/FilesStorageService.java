package tn.esprit.spring.appmed.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import tn.esprit.spring.appmed.model.FileDB;
import tn.esprit.spring.appmed.model.Patient;
import tn.esprit.spring.appmed.repository.FileDBRepository;
import tn.esprit.spring.appmed.repository.PatientRepository;

@Service
public class FilesStorageService {
	private final Path root = Paths.get("uploads/files");

	private static final String MP4_CONTENT_TYPE = "video/mp4";
	private static final String DOCX_CONTENT_TYPE = "document/docx";
	@Autowired
	private FileDBRepository fileDBRepository;
	@Autowired
	private PatientRepository pr;

  public void store(MultipartFile file,int idP) throws IOException {
		  
		  try {
			
			
	    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
	    
	    FileDB FileDB = new FileDB(fileName, file.getContentType(), file.getBytes());
	    
	            fileDBRepository.save(FileDB);

	    		Patient patient =	pr.findById(idP).orElse(null);
	    		FileDB.setPatient(patient);
	   
	    		fileDBRepository.save(FileDB);
	  }catch (MalformedURLException e) {
	      throw new RuntimeException("Error: " + e.getMessage());
	    }
		   }

	public void init() {
		try {
			Files.createDirectory(root);
		} catch (IOException e) {
			throw new RuntimeException("Could not initialize folder for upload!");
		}
	}
	public Set<Patient> getAllPatients() {
		Set<Patient> filelist = new HashSet<>();
		List<Patient> list = new ArrayList<Patient>();
		list = (List<Patient>) pr.findAll();
		for (Patient patient : list) 
			if(patient.getFiles()!=null)
				filelist.add(patient);
		
		return filelist;

	}

	public void save(MultipartFile file) throws IOException {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		FileDB FileDB = new FileDB(fileName, file.getContentType(), file.getBytes());
		try {

			Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
			// copy the file to the upload directory,it will replace any file with same
			// name.
			// Files.copy(file.getInputStream(),
			// this.root.resolve(file.getOriginalFilename()),
			// StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {

			throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
		}
	}

	public Resource load(String filename) {
		try {
			Path file = root.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("Could not read the file!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Error: " + e.getMessage());
		}
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(root.toFile());
	}

	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
		} catch (IOException e) {
			throw new RuntimeException("Could not load the files!");
		}
	}
}
