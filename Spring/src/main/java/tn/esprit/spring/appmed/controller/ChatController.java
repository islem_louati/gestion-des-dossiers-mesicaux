package tn.esprit.spring.appmed.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.spring.appmed.model.Message;

@RestController
public class ChatController {
	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;
	
	
	@MessageMapping("/message")
	@SendTo("/chatroom/public")// app/message
	public Message receivePublicMessage(@Payload Message message) {
		return message;
		}
		@MessageMapping("/private_message")
		public Message receivePrivateMessage(@Payload Message message) {
			simpMessagingTemplate.convertAndSendToUser(message.getReceiverName(),"/private",message);//user/david/private
			return message ;

			
		}
		
		
		
		
	
	

}
