package tn.esprit.spring.appmed.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.appmed.email.EmailSender;
import tn.esprit.spring.appmed.model.AppUser;
import tn.esprit.spring.appmed.model.AppUserRepository;
import tn.esprit.spring.appmed.model.AppUserRole;
import tn.esprit.spring.appmed.model.Docteur;
import tn.esprit.spring.appmed.model.Patient;
import tn.esprit.spring.appmed.repository.DocteurRepository;
import tn.esprit.spring.appmed.repository.PatientRepository;

@Service
public class DocteurService implements DocteurServiceImp {
	@Autowired
	DocteurRepository dr;
	@Autowired
	PatientRepository pr;
	@Autowired
	AppUserRepository userrep;
	@Autowired
	 EmailSender emailSender;
	
	@Override
	public int getcurrentdoctor() {
		Date date = new Date();
		LocalDate localDate = LocalDate.now();
		System.out.println(date.getDay()+"aa");
		int s =0;
		for (Docteur docteur : dr.findAll()) {
			
			if(date.getMonth()== 8 && date.getDay()<30)
			{
				
				s=s+1;
		
			}
		}
		return s;
	}	
	@Override
	public Set<Docteur> getAllDocteurs(int idP) {
		Set<Docteur> hashset = new HashSet<>();
		List<Docteur> list = new ArrayList<Docteur>();
		Patient p = pr.findById(idP).orElse(null);
		list = (List<Docteur>) dr.findAll();
		for (Docteur docteur : dr.findAll()) {
			if (docteur.getPatients().isEmpty())
			{
				 System.err.println(docteur.getIdD()+"y");
				 hashset.add(docteur);
			}
			for (Docteur pp : p.getDocteurs()) {
		
		
		
				
				int s = pp.getIdD();
			
				 if (s != docteur.getIdD() || docteur.getPatients().isEmpty() )
				{
					 System.err.println(docteur.getIdD()+"y");
					hashset.add(docteur);
				}
				

			

			}
		
		}
		return hashset;
	}

	@Override
	public void addDocteur(Docteur docteur) {
		 List<AppUser> c = userrep.findByAppUserRole(AppUserRole.DOCTEUR);
		dr.save(docteur);
	     String link = "http://localhost:3000/registreForDocteur/"+docteur.getIdD() ;
	
	      emailSender.sendUser(
	    		  docteur.getEmail(),
	    		  buildEmail(docteur.getNom(), link));

	}
	@Override
	public void completerprofile(Long id,Docteur docteur) {
		   AppUser a =userrep.findById(id).orElse(null);
			
				// Mission m ;
		
			dr.save(docteur);
				a.setDocteur(docteur);
				userrep.save(a);
				
	
					

	}

	@Override
	public Docteur getDocteurById(int id) {
		return dr.findById(id).get();
	}

	@Override
	public void deleteDocteurById(int id) {
		dr.deleteById(id);

	}

	@Override
	public void updateDocteur(Docteur d, int idD) {
		if (dr.findById(idD).orElse(null) != null)
			d.setIdD(idD);
		dr.save(d);
	}

	@Override
	public void affectPatientForDoctor(int idD, int idP) {
		Patient p = pr.findById(idP).orElse(null);
		Docteur dd = dr.findById(idD).orElse(null);

		p.getDocteurs().add(dd);
		p.getDocteurs().add(dd);
		pr.save(p);

	}

	@Override
	public List<Docteur> getAllDocteurrs() {
		return (List<Docteur>) dr.findAll();
	}

	
	  private String buildEmail(String name, String link) {
	        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
	                "\n" +
	                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
	                "\n" +
	                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
	                "    <tbody><tr>\n" +
	                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
	                "        \n" +
	                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
	                "          <tbody><tr>\n" +
	                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
	                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
	                "                  <tbody><tr>\n" +
	                "                    <td style=\"padding-left:10px\">\n" +
	                "                  \n" +
	                "                    </td>\n" +
	                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
	                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
	                "                    </td>\n" +
	                "                  </tr>\n" +
	                "                </tbody></table>\n" +
	                "              </a>\n" +
	                "            </td>\n" +
	                "          </tr>\n" +
	                "        </tbody></table>\n" +
	                "        \n" +
	                "      </td>\n" +
	                "    </tr>\n" +
	                "  </tbody></table>\n" +
	                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
	                "    <tbody><tr>\n" +
	                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
	                "      <td>\n" +
	                "        \n" +
	                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
	                "                  <tbody><tr>\n" +
	                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
	                "                  </tr>\n" +
	                "                </tbody></table>\n" +
	                "        \n" +
	                "      </td>\n" +
	                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
	                "    </tr>\n" +
	                "  </tbody></table>\n" +
	                "\n" +
	                "\n" +
	                "\n" +
	                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
	                "    <tbody><tr>\n" +
	                "      <td height=\"30\"><br></td>\n" +
	                "    </tr>\n" +
	                "    <tr>\n" +
	                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
	                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
	                "        \n" +
	                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote>\n Link will expire in 15 minutes. <p>See you soon</p>" +
	                "        \n" +
	                "      </td>\n" +
	                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
	                "    </tr>\n" +
	                "    <tr>\n" +
	                "      <td height=\"30\"><br></td>\n" +
	                "    </tr>\n" +
	                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
	                "\n" +
	                "</div></div>";
	    }

    
	    
	    
	    
	    
}
