package tn.esprit.spring.appmed.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.esprit.spring.appmed.model.Docteur;


	
	@Repository
	public interface DocteurRepository extends CrudRepository<Docteur, Integer> {

}
