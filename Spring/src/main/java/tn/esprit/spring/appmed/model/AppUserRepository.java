package tn.esprit.spring.appmed.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface AppUserRepository
        extends JpaRepository<AppUser, Long> {

AppUser findByEmail(String email);
List<AppUser> findByenabled(Boolean enabled);
    List<AppUser> findByAppUserRole(AppUserRole appUserRole);
	Boolean existsByUsername(String username);
	List<AppUser> findByUsername(String username);
	
    @Transactional
    @Modifying
    @Query("UPDATE AppUser a " +
            "SET a.enabled = TRUE WHERE a.email = ?1")
    int enableAppUser(String email);
    

	  Optional<AppUser> getByEmail(String email);
	  Optional<AppUser> getByUsername(String username);
	  


	    @Query(value = "SELECT u.firstName, u.lastname FROM AppUser u WHERE u.id = :userId", nativeQuery = true)
	    String getUsernameByUserId(@Param(value = "userId") int id);
	    
	    

}
