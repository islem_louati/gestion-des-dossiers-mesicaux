package tn.esprit.spring.appmed.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Docteur implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idD;
	
	private String nom;
	
	private String prenom;
	private String email;
	
	private String specialité;
	private int téléphone ;
//	
	
	@OneToOne(mappedBy = "docteur")
	@JsonIgnore
	private AppUser appuser;
	 
	
	  @ManyToMany(fetch = FetchType.LAZY,
		      cascade = {
		          CascadeType.PERSIST,
		          CascadeType.MERGE
		      },
		      mappedBy = "docteurs")
		  @JsonIgnore
		  private Set<Patient> patients = new HashSet<>();
	
	public Docteur() {
		this.idD = 0;
		this.nom = "";
		this.prenom = "";
		this .specialité = "";
		this.téléphone=0;
	}
	

	public Docteur(int idD, String nom, String prenom, String email, String specialité, int téléphone) {
		super();
		this.idD = idD;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.specialité = specialité;
		this.téléphone = téléphone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return "Docteur [idD=" + idD + ", nom=" + nom + ", prenom=" + prenom + ", specialité=" + specialité
				+ ", téléphone=" + téléphone + ", patients=" + patients + "]";
	}


	public Docteur(int id, String nom, String prenom, String specialité, int téléphone) {
		super();
		this.idD = id;
		this.nom = nom;
		this.prenom = prenom;
		this.specialité = specialité;
		this.téléphone = téléphone;
	}



	public int getIdD() {
		return idD;
	}


	public void setIdD(int idD) {
		this.idD = idD;
	}




	public String getNom() {
		return nom;
	}









	public void setNom(String nom) {
		this.nom = nom;
	}









	public String getPrenom() {
		return prenom;
	}









	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}









	public String getSpecialité() {
		return specialité;
	}









	public void setSpecialité(String specialité) {
		this.specialité = specialité;
	}









	public int getTéléphone() {
		return téléphone;
	}









	public void setTéléphone(int téléphone) {
		this.téléphone = téléphone;
	}


	public Set<Patient> getPatients() {
		return patients;
	}


	public void setPatients(Set<Patient> patients) {
		this.patients = patients;
	}









	
	

}
