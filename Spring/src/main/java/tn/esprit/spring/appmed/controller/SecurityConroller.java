package tn.esprit.spring.appmed.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SecurityConroller {
	
	@GetMapping("/")
	public String testApp() {
		return "hello Spring Security ";
	}

}
