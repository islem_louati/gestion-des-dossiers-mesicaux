package tn.esprit.spring.appmed.service;


import tn.esprit.spring.appmed.controller.ConfirmationToken;
import tn.esprit.spring.appmed.controller.ConfirmationTokenService;
import tn.esprit.spring.appmed.model.AppUser;
import tn.esprit.spring.appmed.model.AppUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.security.crypto.password.PasswordEncoder;
@Service
@Transactional
public class AppUserService {

	private final static String USER_NOT_FOUND_MSG =
            "user with email %s not found";

    private final AppUserRepository appUserRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ConfirmationTokenService confirmationTokenService;
	public AppUserService(AppUserRepository appUserRepository,BCryptPasswordEncoder bCryptPasswordEncoder,ConfirmationTokenService confirmationTokenService) {
		this.appUserRepository = appUserRepository;
		this.bCryptPasswordEncoder=bCryptPasswordEncoder;
		this.confirmationTokenService=confirmationTokenService;
	
	}

//    @Override
//    public UserDetails loadUserByUsername(String username)
//            throws UsernameNotFoundException {
//        return appUserRepository.findByUsername(username)
//                .orElseThrow(() ->
//                        new UsernameNotFoundException(
//                                String.format(USER_NOT_FOUND_MSG, username)));
//    }
    public String findUsernameById(int id) {
        return appUserRepository.getUsernameByUserId(id);
    }
    public void findById(Long userId) {
         appUserRepository.findById(userId);
    }
    public String signUpUser(AppUser appUser) {
        boolean userExists = appUserRepository
                .getByEmail(appUser.getEmail())
                .isPresent();
        boolean userExistss = appUserRepository
                .getByUsername(appUser.getUsername())
                .isPresent();
String message="";


        if (userExists) {
            // TODO check of attributes are the same and
            // TODO if email not confirmed send confirmation email.
       	 message = "email exist " + appUser.getEmail();
             throw new IllegalStateException(message);     
        }
             else if (userExistss) {
            // TODO check of attributes are the same and
            // TODO if email not confirmed send confirmation email.
       	 message = "username exist " + appUser.getUsername();
            throw new IllegalStateException(message);     
        }
System.out.println(appUser.getPassword()+"zert");
       

        String encodedPassword = bCryptPasswordEncoder
                .encode(appUser.getPassword());

        appUser.setPassword(encodedPassword);
     
        appUser.getAppUserRole();
       
        appUserRepository.save(appUser);

        String token = UUID.randomUUID().toString();

        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                appUser
        );

        confirmationTokenService.saveConfirmationToken(
                confirmationToken);

//        TODO: SEND EMAIL

        return token;
    }
    
    public String signUpUserr(Long id,String password) {

    	AppUser aa = appUserRepository.findById(id).orElse(null);

        String encodedPassword = bCryptPasswordEncoder
                .encode(password);

        aa.setPassword(encodedPassword);

       
        appUserRepository.save(aa);

        String token = UUID.randomUUID().toString();

        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                aa
        );

        confirmationTokenService.saveConfirmationToken(
                confirmationToken);

//        TODO: SEND EMAIL

        return token;
    }
    
    
    public void signUpUserforforget(Long id,AppUser appUser) {
  
     	AppUser aaa = appUserRepository.findById(id).orElse(null);
        String encodedPassword = bCryptPasswordEncoder
                .encode(aaa.getPassword());

        aaa.setPassword(encodedPassword);
     
       
       
        appUserRepository.save(aaa);

      

//        TODO: SEND EMAIL

        
    }

    public int enableAppUser(String email) {
        return appUserRepository.enableAppUser(email);
        
    }

	
}