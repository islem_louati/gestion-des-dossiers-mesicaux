package tn.esprit.spring.appmed.model;

public enum AppUserRole {
    DOCTEUR,
    ADMIN,
    Patient
}
