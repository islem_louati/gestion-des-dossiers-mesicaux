package tn.esprit.spring.appmed.service;

import java.util.List;
import java.util.Set;

import tn.esprit.spring.appmed.model.Docteur;
import tn.esprit.spring.appmed.model.Patient;


public interface DocteurServiceImp {
	Set<Docteur> getAllDocteurs( int idD);
	List<Docteur> getAllDocteurrs();
	void addDocteur(Docteur docteur);
	int getcurrentdoctor();
	void completerprofile(Long id,Docteur docteur);
	Docteur getDocteurById(int id);
	void deleteDocteurById(int id);
	void updateDocteur(Docteur d, int idD);
	void affectPatientForDoctor(int idD, int idP);
	

}
