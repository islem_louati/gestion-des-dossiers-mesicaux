package tn.esprit.spring.appmed.config;

import java.util.List;

import tn.esprit.spring.appmed.model.AppUserRole;


public class JwtResponse {
  private String token;
  private String type = "Bearer";
  private Long id;
  private String username;
  private String email;
private AppUserRole appUserRole;;
private Boolean  enabled ; 





public JwtResponse(String token, Long id, String username, String email, AppUserRole appUserRole) {
	super();
	this.token = token;
	this.id = id;
	this.username = username;
	this.email = email;
	this.appUserRole = appUserRole;
}




public JwtResponse(String token, Long id, String username, String email, Boolean enabled, AppUserRole appUserRole) {
	super();
	this.token = token;
	this.id = id;
	this.username = username;
	this.email = email;
	this.appUserRole = appUserRole;
	this.enabled=enabled;
}




public JwtResponse(String token, Long id, String username, String email, AppUserRole appUserRole, Boolean enabled) {
	super();
	this.token = token;
	this.id = id;
	this.username = username;
	this.email = email;
	this.appUserRole = appUserRole;
	this.enabled = enabled;
}




public JwtResponse(Long id, String username, String email, AppUserRole appUserRole, Boolean enabled) {
	super();
	this.id = id;
	this.username = username;
	this.email = email;
	this.appUserRole = appUserRole;
	this.enabled = enabled;
}




public JwtResponse(String username, String email, AppUserRole appUserRole) {
	super();
	this.username = username;
	this.email = email;
	this.appUserRole = appUserRole;
}







public String getAccessToken() {
    return token;
  }

  public void setAccessToken(String accessToken) {
    this.token = accessToken;
  }

  public String getTokenType() {
    return type;
  }

  public void setTokenType(String tokenType) {
    this.type = tokenType;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

 



public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public AppUserRole getAppUserRole() {
	return appUserRole;
}

public void setAppUserRole(AppUserRole appUserRole) {
	this.appUserRole = appUserRole;
}

  
}

