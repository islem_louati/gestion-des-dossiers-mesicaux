package tn.esprit.spring.appmed.service;

import java.util.List;
import java.util.Set;

import tn.esprit.spring.appmed.model.Patient;

public interface PatientServiceImp {
	List<Patient> getAllPatients();
	List<Patient> filtrePatient();
	Set<Patient> patientAvecDocteur();
	Set<Patient> patientSansDocteur();
	void patientUrgent();
	void addPatient(Patient patient);
	Patient getPatientById(int id);
	void deletePatientById(int idP);
	void dessafect(int idP);
	void updatePatient(Patient pt, int idPa);
	void affecterFileForPatient(int idP, int idF);
	
	

}
