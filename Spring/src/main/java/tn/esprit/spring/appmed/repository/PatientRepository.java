package tn.esprit.spring.appmed.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.esprit.spring.appmed.model.Patient;



@Repository
public interface PatientRepository extends CrudRepository<Patient, Integer> {

	@Query("select p from Patient p where p.Urgent=true  order by p.Age DESC")
	List<Patient> getpatientByAge(Boolean Urgent);

}
