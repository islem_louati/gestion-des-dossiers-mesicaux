package tn.esprit.spring.appmed.config;


import tn.esprit.spring.appmed.controller.*;
import tn.esprit.spring.appmed.service.AppUserService;
import tn.esprit.spring.appmed.service.UserDetailsServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
@EnableAspectJAutoProxy

@EnableGlobalMethodSecurity(
      //  securedEnabled = true,
      //  jsr250Enabled = true,
        prePostEnabled = true
)

@ComponentScan(basePackages = { "com.example.demo.queries" })
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	UserDetailsServiceImpl userDetailsService;

	private final AppUserService appUserService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
	public WebSecurityConfig(AppUserService appUserService,BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.appUserService = appUserService;
		this.bCryptPasswordEncoder=bCryptPasswordEncoder;
	}


	@Autowired
	private AuthEntryPointJwt unauthorizedHandler;
	@Bean
	public AuthTokenFilter authenticationJwtTokenFilter() {
		return new AuthTokenFilter();
	}
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
      
    	http.cors().and().csrf().disable()
    	.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
    	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
    	.authorizeRequests()
    	.antMatchers("/api/v1/registration/**").permitAll()
    	.antMatchers("/cliens/affect/**").permitAll()
    	.antMatchers("/questions/**/**").permitAll()
    	.antMatchers("/docteur/affectpatientTodoctor/**").permitAll()
    	.antMatchers("/patient/dessafec/**").permitAll()
    	.antMatchers("/patient/deletePatientById/**").permitAll()
    	.antMatchers("/upload/**").permitAll()
    	.antMatchers("/ws/**").permitAll()
    	.antMatchers("/swagger-ui.html/**/**/**").permitAll()
    	
    	.and()
    	.authorizeRequests()
    	.antMatchers("patient/**").permitAll()
    	.antMatchers("candidats/**").permitAll()
    	.anyRequest().authenticated();
    	
    http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);

    }
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
		
				registry.addMapping("/**")
			
			
				.allowedOrigins("*")
				.allowedMethods("*");
			}
		};
	}

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
	

//    @Bean
//    public DaoAuthenticationProvider daoAuthenticationProvider() {
//        DaoAuthenticationProvider provider =
//                new DaoAuthenticationProvider();
//        provider.setPasswordEncoder(bCryptPasswordEncoder);
//        provider.setUserDetailsService(appUserService);
//        return provider;
//    }
}
