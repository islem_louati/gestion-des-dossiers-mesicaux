package tn.esprit.spring.appmed.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "files")
public class FileDB {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idF;
  private String name;
  private String type;
  @Lob
  private byte[] data;

  public FileDB() {
  }
  public FileDB(String name, String type, byte[] data) {
    this.name = name;
    this.type = type;
    this.data = data;
  }
  @ManyToOne()

  private Patient patient;

  public int getId() {
	return idF;
}
public void setId(int id) {
	this.idF = id;
}

public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }
  public byte[] getData() {
    return data;
  }
  public void setData(byte[] data) {
    this.data = data;
  }
public void setPatient(Patient patient) {
	this.patient=patient;
	
}
public Patient getPatient() {
	return patient;
}

}