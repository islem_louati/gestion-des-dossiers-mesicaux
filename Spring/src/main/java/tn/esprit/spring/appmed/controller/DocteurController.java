package tn.esprit.spring.appmed.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.spring.appmed.model.Docteur;

import tn.esprit.spring.appmed.service.DocteurServiceImp;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/docteur")
public class DocteurController {
	@Autowired
	DocteurServiceImp ds;
	
	@GetMapping(value = "/getAllDocteurs/{idP}")
	@ResponseBody
	public Set<Docteur> getAllDocteurs(@PathVariable int idP) {
		return ds.getAllDocteurs(idP);
	}
	@GetMapping(value = "/getAllDocteurs")
	@ResponseBody
	public List<Docteur> getAllDocteurs() {
		return ds.getAllDocteurrs();
	}
	
	@GetMapping(value = "/getDocteurById/{id}")
	@ResponseBody
	public Docteur getDocteurById(@PathVariable("id") int idD) {
		return ds.getDocteurById(idD);
	}
	@GetMapping("/displayBarGraph")
	public Map <String,Integer> barGraph(Model model) {
		Map<String, Integer> surveyMap = new LinkedHashMap<>();
		surveyMap.put("janvier", 5);
		surveyMap.put("Février", 8);
		surveyMap.put("Mars", 10);
		surveyMap.put("Avril", 15);
		surveyMap.put("mai", 10);
		surveyMap.put("juin", 12);
		surveyMap.put("juillet", 24);
		surveyMap.put("Aout", 20);
		surveyMap.put("Sepbtember", ds.getcurrentdoctor());
		surveyMap.put("October", 0);
		surveyMap.put("Novenmber", 0);
		surveyMap.put("Décenmber", 0);
		model.addAttribute("surveyMap", surveyMap);
		return surveyMap;
	}
	@GetMapping(value = "/getcuurnt")
	@ResponseBody
	public int getcurrent() {
		return ds.getcurrentdoctor();
	}

	@PostMapping("/addDocteur/{id}")
	@ResponseBody
	public Docteur CompleterDocteur(@RequestBody Docteur docteur, @PathVariable Long id) {
		ds.completerprofile(id,docteur);
		return docteur;

	}
	@PostMapping("/addDocteur")
	@ResponseBody
	public Docteur addDocteur(@RequestBody Docteur docteur) {
		ds.addDocteur(docteur);
		return docteur;

	}
	
	@PostMapping("/affectpatientTodoctor/{idD}/{idP}")
	@ResponseBody
	public void affectpatient(@PathVariable int idD, @PathVariable int idP) {
		ds.affectPatientForDoctor(idP,idD);
		

	}
	@DeleteMapping("/deleteDocteurById/{idD}")
	@ResponseBody
	public void deleteDocteurById(@PathVariable("idD") int idDoc) {

		ds.deleteDocteurById(idDoc);

	}
	@PutMapping(value = "updateDocteur/{idD}")
	public void updateDocteur(@RequestBody Docteur docteur, @PathVariable int idD) {
		ds.updateDocteur(docteur, idD);
	}
	
}
