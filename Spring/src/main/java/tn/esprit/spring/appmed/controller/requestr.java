package tn.esprit.spring.appmed.controller;

import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import tn.esprit.spring.appmed.model.AppUserRole;
import tn.esprit.spring.appmed.model.Docteur;
import tn.esprit.spring.appmed.model.Patient;


public class requestr {
    
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	@NotNull
    @Size(max=30)
	@Min(value=3)
	@NotEmpty(message = "entrer votre email")	
	private String username;
	   private Boolean enabled = true;
	@Email
	private String email;
	private String password;
    @Enumerated(EnumType.STRING)
    private AppUserRole appUserRole;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idD", referencedColumnName = "idD")
	private Docteur docteur;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idP", referencedColumnName = "idP")
	private Patient patient;
	


	public Docteur getDocteur() {
		return docteur;
	}

	public Docteur setDocteur(Docteur docteur) {
		return this.docteur = docteur;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	

	public requestr(@NotNull @Size(max = 30) @Min(3) @NotEmpty(message = "entrer votre email") String username,
			Boolean enabled, @Email String email, String password, AppUserRole appUserRole, Docteur docteur) {
		super();
		this.username = username;
		this.enabled = enabled;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		this.docteur = docteur;
	}

	public requestr(Long id,
			@NotNull @Size(max = 30) @Min(3) @NotEmpty(message = "entrer votre email") String username,
			@Email String email, String password, AppUserRole appUserRole
		) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}





	public AppUserRole getAppUserRole() {
		return appUserRole;
	}

	public void setAppUserRole(AppUserRole appUserRole) {
		this.appUserRole = appUserRole;
	}


	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public requestr(Boolean enabled, Long id,
			@NotNull @Size(max = 30) @Min(3) @NotEmpty(message = "entrer votre email") String username,
			@Email String email, String password, AppUserRole appUserRole
		) {
		super();
		this.enabled = enabled;
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
	
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public requestr() {
		super();
	}

	@Override
	public String toString() {
		return "RegistrationRequest [username=" + username + ", email=" + email
				+ ", password=" + password + ", appUserRole=" + appUserRole + "]";
	}


}

