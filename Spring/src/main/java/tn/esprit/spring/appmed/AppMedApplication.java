package tn.esprit.spring.appmed;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;



@SpringBootApplication
@EnableScheduling
public class AppMedApplication {
	

	

	public static void main(String[] args) {
		SpringApplication.run(AppMedApplication.class, args);
		
	}
	
	
	
}
