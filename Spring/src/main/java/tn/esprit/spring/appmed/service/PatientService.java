package tn.esprit.spring.appmed.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.appmed.model.Docteur;
import tn.esprit.spring.appmed.model.FileDB;
import tn.esprit.spring.appmed.model.Patient;
import tn.esprit.spring.appmed.repository.FileDBRepository;
import tn.esprit.spring.appmed.repository.PatientRepository;
import org.springframework.scheduling.annotation.Scheduled;

@Service
public class PatientService implements PatientServiceImp {
	@Autowired
	private FileDBRepository fileDBRepository;

	@Autowired
	PatientRepository pr;

	@Override
	public List<Patient> getAllPatients() {
		List<Patient> list = new ArrayList<Patient>();
		list = (List<Patient>) pr.findAll();
		return list;

	}

	@Override
	public Patient getPatientById(int id) {
		return pr.findById(id).get();
	}

	@Override
	public void addPatient(Patient patient) {
		pr.save(patient);
	}

	@Override
	public void deletePatientById(int idP) {
	pr.deleteById(idP);
		
	}

		@Override
		public void dessafect(int idP) {
			Patient p = pr.findById(idP).orElse(null);
	
		}
		

	

	@Override
	public void updatePatient(Patient pt, int idPa) {
		if (pr.findById(idPa).orElse(null) != null)
			pt.setIdP(idPa);
		pr.save(pt);
	}
	@Override
	public void affecterFileForPatient(int idP, int idF) {
	FileDB f = fileDBRepository.findById(idF).orElse(null);
	Patient p = pr.findById(idP).orElse(null);
	f.setPatient(p);
fileDBRepository.save(f);
		
	}

	@Override
	public Set<Patient> patientAvecDocteur() {
		Set<Patient> hashset = new HashSet<>();
		for (Patient p : pr.findAll()) {
			if(p.getDocteurs()!=null)
			{
			hashset.add(p);	
			}
		}
		return hashset;
	}

	@Override
	public Set<Patient> patientSansDocteur() {
		Set<Patient> hashset = new HashSet<>();
		for (Patient p : pr.findAll()) {
			if(p.getDocteurs()==null)
			{
			hashset.add(p);	
			}
		}
		return hashset;
	}

	@Override
	public List<Patient> filtrePatient() {
	
	return pr.getpatientByAge(Boolean.TRUE);
	}
	
	@Override
	@Scheduled(cron = "*/10 * * * * *")
	public void patientUrgent() {
		for (Patient p : pr.findAll()) {
			
			if(p.getFiles().isEmpty())
			{
				System.out.println("AA");
				p.setUrgent(false);

				pr.save(p);
			}
		}
	}


}
