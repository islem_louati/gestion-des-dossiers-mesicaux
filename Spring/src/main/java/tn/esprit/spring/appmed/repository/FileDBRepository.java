package tn.esprit.spring.appmed.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.esprit.spring.appmed.model.FileDB;

@Repository
public interface FileDBRepository extends JpaRepository<FileDB, Integer> {
	
	Boolean existsByname(String name);
	FileDB getByname(String name);
	Optional<FileDB> findByname(String name);

}
