package tn.esprit.spring.appmed.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import tn.esprit.spring.appmed.model.FileDB;
import tn.esprit.spring.appmed.model.FileInfo;
import tn.esprit.spring.appmed.model.Patient;
import tn.esprit.spring.appmed.model.ResponseMessage;
import tn.esprit.spring.appmed.repository.FileDBRepository;
import tn.esprit.spring.appmed.repository.PatientRepository;
import tn.esprit.spring.appmed.service.FilesStorageService;
import tn.esprit.spring.appmed.service.PatientServiceImp;

@RestController
@CrossOrigin(origins = "http://localhost:3000")

public class FilesController {
	@Autowired
	FilesStorageService storageService;
	@Autowired
	FileDBRepository repo;
	@Autowired
	PatientServiceImp ps;
	@Autowired
	PatientRepository pr;
	private static final String MP4_CONTENT_TYPE = "video/mp4";
	private static final String DOCX_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
	private static final String PDF_CONTENT_TYPE = "application/pdf";

	
	@GetMapping("/allfilesbysearch/{name}")
	public Set<FileDB> getListFilebysearch(@PathVariable String name) throws IOException {
		Set<FileDB> fileList = new HashSet<>();
		
		String l = "islem";
		String message = "";
		String s = "";
	List<Patient> p= ps.getAllPatients();
		
		boolean found = true;
		File dir = new File("uploads/files");
		File[] files = dir.listFiles();
		PDDocument document = null;
		for (FileDB ff : repo.findAll()) {

		for (File file : files) {
			if (file.isFile()) {
			
			if (file.getName().equals(ff.getName())) {
			
					BufferedReader inputStream = null;
					String line;

					FileInputStream fis = new FileInputStream(file);
					document = PDDocument.load(fis);
					

					PDFTextStripper pdfTextStripper = new PDFTextStripper();
					pdfTextStripper.setStartPage(1); // comment this line if you want to read the entire
														// document
					pdfTextStripper.getEndPage(); // comment this line if you want to read the entire document
					String docText = pdfTextStripper.getText(document);

					if (docText.contains(name)) {
						System.out.println("eee");
						fileList.add((ff));
					Patient pp=	ff.getPatient();
					pp.setUrgent(true);
					pp.setMaladie(name);
					pr.save(pp);
			

					}
					document.close();}}
		}}
return fileList;
		}
	
	
	
	@PostMapping("/upload/{idP}")
	public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable int idP)
			throws IOException {

		boolean fileExists = repo.findByname(file.getName()).isPresent();
		String message = "";
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		FileDB FileDB = new FileDB(fileName, file.getContentType(), file.getBytes());
		System.out.println("rrrr" + FileDB.getType());

		if (!fileExists) {
			if (FileDB.getType().equals(PDF_CONTENT_TYPE) || FileDB.getType().equals(DOCX_CONTENT_TYPE)) {

				System.out.println("iss" + FileDB.getType());
				try {
					storageService.save(file);
					storageService.store(file, idP);

					message = "Uploaded the file successfully: " + file.getOriginalFilename();
					return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ResponseMessage(message));

				}

				catch (Exception e) {

				}
			}
			message = "ce type de fichier ne peut pas etre telechargé: " + file.getOriginalFilename();
			return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(new ResponseMessage(message));
		}

		System.out.println("iss" + FileDB.getType());
		message = "ce fichier existe déja : " + file.getOriginalFilename() + "!";
		return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(new ResponseMessage(message));

	}

	@GetMapping("/allfiles")
	public Set<Patient> findall() {
		return storageService.getAllPatients();
	}
	@GetMapping("/allfile")
	public List<FileDB> findalll() {
		return repo.findAll();
	}

	@DeleteMapping("/deletefile/{idP}/{id}")
	public void search(@PathVariable int idP, @PathVariable int id) {
		File dir = new File("uploads/files");
		File[] files = dir.listFiles();
		Patient candidat = pr.findById(idP).orElse(null);
		candidat.setFiles(null);
		pr.save(candidat);
		FileDB fo = repo.findById(id).orElse(null);
		for (FileDB ff : repo.findAll()) {
			for (File file : files) {
				repo.delete(fo);
				if (ff.getName().equals(file.getName())) {
					FileSystemUtils.deleteRecursively(file);

				}

			}
		}
	}

	@GetMapping("/files")
	public ResponseEntity<List<FileInfo>> getListFiles() {
		List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
			String filename = path.getFileName().toString();
			String url = MvcUriComponentsBuilder
					.fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();
			return new FileInfo(filename, url);
		}).collect(Collectors.toList());
		return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable String filename) {
		Resource file = storageService.load(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
	
	@GetMapping("/allfiles/{name}")
	public Set<String> getListFile(@PathVariable String name) throws IOException {
		Set<String> fileList = new HashSet<>();
	
		FileDB f=  repo.getByname(name);
		System.out.println(f.getName());
		boolean found = true;
		File dir = new File("uploads/files");
		File[] files = dir.listFiles();
		PDDocument document = null;
		for (FileDB ff : repo.findAll()) {
		for (File file : files) {
			if (file.isFile()) {
				if (ff.getName().equals(f.getName()))
				{	
			if (file.getName().equals(ff.getName())) {
				
					BufferedReader inputStream = null;
					String line;

					FileInputStream fis = new FileInputStream(file);
					document = PDDocument.load(fis);
					

					PDFTextStripper pdfTextStripper = new PDFTextStripper();
					pdfTextStripper.setStartPage(1); // comment this line if you want to read the entire
														// document
					pdfTextStripper.getEndPage(); // comment this line if you want to read the entire document
					String docText = pdfTextStripper.getText(document);
							
						
						System.out.println(docText+"aaa");
						
fileList.add(docText);
					}
					document.close();}}
		}}
return fileList;
		}

}
