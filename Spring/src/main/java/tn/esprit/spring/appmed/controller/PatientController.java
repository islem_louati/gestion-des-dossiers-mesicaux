package tn.esprit.spring.appmed.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.spring.appmed.model.Docteur;
import tn.esprit.spring.appmed.model.Patient;
import tn.esprit.spring.appmed.repository.DocteurRepository;
import tn.esprit.spring.appmed.repository.PatientRepository;
import tn.esprit.spring.appmed.service.PatientServiceImp;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/patient")
public class PatientController {

	@Autowired
	PatientServiceImp ps;

	@Autowired
	PatientRepository pr;
	
	@Autowired
	DocteurRepository dr;
	
	@GetMapping(value = "/getAllPatients")
	@ResponseBody
	public List<Patient> getAllPatients() {
		return ps.getAllPatients();
	}
	
	@GetMapping(value = "/PatientAvecDocteur")
	@ResponseBody
	public Set<Patient> getPatientAvecDocteur() {
		return ps.patientAvecDocteur();
	}
	@GetMapping(value = "/getPatientSansDocteur")
	@ResponseBody
	public Set<Patient> getPatientSansDocteur() {
		return ps.patientSansDocteur();
	}

	@GetMapping(value = "/getPatientById/{id}")
	@ResponseBody
	public Patient getPatientById(@PathVariable("id") int idP) {
		return ps.getPatientById(idP);
	}
	
	@GetMapping(value = "/getpatientfiltre")
	@ResponseBody
	public List<Patient> getpatientfiltre() {
		
		return ps.filtrePatient();
	}

	@PostMapping("/addPatient")
	@ResponseBody
	public Patient addPatient(@RequestBody Patient patient) {
		ps.addPatient(patient);
		return patient;

	}

	@DeleteMapping("/deletePatientById/{idP}")
	@ResponseBody
	public void deletePatientById(@PathVariable("idP") int idP) {

		ps.deletePatientById(idP);

	}
	@PostMapping("/dessafec/{idP}")
	@ResponseBody
	public void dessafect(@PathVariable("idP") int idP) {

		ps.dessafect(idP);

	}

	@PutMapping(value = "updatePatient/{idP}")
	public void updatePatient(@RequestBody Patient patient, @PathVariable int idP) {
		ps.updatePatient(patient, idP);
	}
	@PostMapping("/affectfileTopatient/{idP}/{idF}")
	@ResponseBody
	public void affecterFileForPatient(@PathVariable int idP, @PathVariable int idF) {
		ps.affecterFileForPatient(idP,idF);

}
	


}
