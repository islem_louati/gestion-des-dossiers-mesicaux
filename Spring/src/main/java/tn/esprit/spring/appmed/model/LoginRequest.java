package tn.esprit.spring.appmed.model;

import javax.validation.constraints.NotBlank;

public class LoginRequest {

	@NotBlank
    private String username;


	@NotBlank
	private String password;
	 private Boolean enabled ;
	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}

	
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LoginRequest(@NotBlank String username, @NotBlank String password, Boolean enabled) {
		super();
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}



}
