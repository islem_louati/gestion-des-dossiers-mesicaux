//package tn.esprit.spring.appmed.controller;
//
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//import java.security.Principal;
//
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import tn.esprit.spring.appmed.config.JWTTokenHelper;
//import tn.esprit.spring.appmed.model.User;
//import tn.esprit.spring.appmed.requests.AuthenticationRequest;
//import tn.esprit.spring.appmed.responses.LoginResponse;
//
//@RestController
//@RequestMapping("/api/v1")
//@CrossOrigin(origins = "http://localhost:3000")
//public class AuthenticationController {
//	
//	@Autowired
//	private AuthenticationManager authenticationManager;
//	
//	@Autowired
//     JWTTokenHelper jwtTokenHelper;
//	
//	@Autowired
//	private UserDetailsService userDetailsService ;
//
//	@PostMapping("/auth/login")
//	public ResponseEntity<?> login(@RequestBody AuthenticationRequest authenticationRequset) throws InvalidKeyException, NoSuchAlgorithmException{
//		final Authentication authentication=authenticationManager.authenticate(
//		new UsernamePasswordAuthenticationToken(authenticationRequset.getUserName(), authenticationRequset.getPassword())
//		);
//		SecurityContextHolder.getContext().setAuthentication(authentication);
//		User user=(User)authentication.getPrincipal();
//		
//		String jwtToken=jwtTokenHelper.generateToken(user.getUserName());
//		LoginResponse response=new LoginResponse();
//		response.setToken(jwtToken);
//		return ResponseEntity.ok(response);
//		
// }
//	@GetMapping("auth/userinfo")
//	public ResponseEntity<?>getUserInfo(Principal user){
//		User userObj=(User) userDetailsService.loadUserByUsername(user.getName());
//		return null ;
//		
//	}
//     }
